<?php
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*

Program     : 
Description : 
Programmer  : Thrinai Chankong

Email       : thrinai@hotmail.com
Website     : http://www.plapayoon.com

Date        : 
Modify log  : 

*/
/*------------------------------------------------------------------*/
/*- Require --------------------------------------------------------*/


include("../module/SiXhEaD.Template.php");
include("../module/SiXhEaD.Pagination.php");
include("../module/_config.php");
include("../module/_module.php");
/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/

$tp_index	 =	"_tp_index.html";


/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/

$strLogInBar	=	 CheckLogin();

/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/

$CateId	=	$_REQUEST["CateId"];
$LimitPage		=	$_REQUEST['LimitPage'];
$page			=	$_REQUEST['page'];

/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/

$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
if (!$conn) {die('Not connected : ' . mysql_error());}
// make foo the current db
$db_selected = mysql_select_db($strCfgDbName, $conn);
if (!$db_selected) { die ('Can\'t use $strCfgDbName : ' . mysql_error());}
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");
$tp = new Template($tp_index);
$sql	=	"SELECT `SubCateId`,`SubCateName` FROM $strCfgDbTableSubCategory WHERE `CateId`='1' AND (IsDelete='N' AND Active='Y') ORDER BY `LineNo`,`SubCateId` 
ASC";
$result	=	mysql_query($sql);
$tp->block("ListCate");
$i=0;
while($row=mysql_fetch_assoc($result)){
	$SubCateId		= $row[SubCateId];
	$SubCateName	=	$row[SubCateName];
	if(($CateId=="")&&($i==0))$CateId=$SubCateId;
	$i++;
	$tp->apply();
}

$sql="SELECT `SubCateId`,`SubCateName` FROM $strCfgDbTableSubCategory WHERE `SubCateId`='$CateId' AND (IsDelete='N' AND Active='Y') ORDER BY `LineNo`,`SubCateId` 
ASC";
$result	=	mysql_query($sql);
while($row=mysql_fetch_assoc($result)){
	
	$CateName	=	$row[SubCateName];
	$tp->apply();
}
$order_by	=	" LineNo,`ProductCode` ASC";
if(!$LimitPage){$LimitPage=20;}
$sp	= New Pagination();
$sp->db_type			=	"MySQL";
$sp->db_table			=	$strCfgDbTableProduct;
$sp->order_by			=	$order_by;
$sp->primary_key	=	"`ProductId`";
$sp->select			=	"DISTINCT `ProductCode`,`ProductName`,`Price`,`AfterDisPrice`,`File1`";
$sp->custom_sql		=	"  WHERE (IsDelete='N' AND `Active`='Y') AND `SubCateId`='$CateId' GROUP BY `ProductCode`" ; # WHERE, JOIN
$sp->custom_param	=	"&LimitPage=$LimitPage&CateId=$CateId&searchq=$searchq";
$sp->per_page		=	$LimitPage;
$sp->order_param	=	"order"; # $_GET["order"];
$sp->page_param	=	"page"; # $_REQUEST["p"];
$sp->page_prev		=	"&#8249; Prev";
$sp->page_next		=	"Next &#8250;";
$sp->page_first		=	"&laquo; หน้าแรก";
$sp->page_last		=	"หน้าสุดท้าย &raquo;";
$sp->sign_left			=	"";
$sp->sign_right		=	" |";
$sp->sign_trim		=	FALSE;
$sp->style				=	1;
$sp->start();

$intRecordAll		=	$sp->get_record_all();
$sql				=	$sp->get_sql();
$strPageLink		=	$sp->get_page_link();

$bg_color = "";
$result = mysql_query($sql);// echo $sql;
if (!$result) { echo "$sql"; die('Invalid query: ' . mysql_error()); }
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");

$cols	=	4;
$tp->block("ListData");
$tp->sub($cols);
$i=0;
$strTbl	=	"";
while($row = mysql_fetch_array($result)) {
	$ProductCode		=	$row[ProductCode];
	$ProductName		=	$row['ProductName'];
	$Price				=	$row['Price'];
	$AfterDisPrice		=	$row['AfterDisPrice'];
	$File1				=	$row['File1'];
	if($AfterDisPrice!="0.00")$Price="<span style=\"text-decoration:line-through;\"  class=\"text_13b_1\">".$Price." thb</span> &nbsp;<b>$AfterDisPrice</b>";
	$ImgProduct = <<<img
../module/phpThumb/phpThumb.php?src=../../content/product/$File1&w=$strCfgProductThumbWidth&h=$strCfgProductThumbHeight&zc=1&sx=0
img;
	$tp->apply();
	$i++;
}
if(intval($i%$cols)>0){
	$intBlank	 =	($cols-intval($i%$cols));
	for($j=0;$j<$intBlank;$j++){
		$strTbl	.=	<<<strdata
			<td height="260" align="center" valign="top"><table width="185" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td><img src="../images/Share/spacer.gif" width="183" height="7" /></td>
                                        </tr>
              </table></td>
			
strdata;
	}
	$strTbl	.=	<<<strdata

                                      </tr>
                                  </table>

										</td>
                                </tr>
strdata;
}


mysql_close($conn);
$tp->Display();
exit;


/*------------------------------------------------------------------*/
?>
