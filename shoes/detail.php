<?php
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*

Program     : 
Description : 
Programmer  : Thrinai Chankong

Email       : thrinai@hotmail.com
Website     : http://www.plapayoon.com

Date        : 
Modify log  : 

*/
/*------------------------------------------------------------------*/
/*- Require --------------------------------------------------------*/


include("../module/SiXhEaD.Template.php");
include("../module/SiXhEaD.Pagination.php");
include("../module/_config.php");
include("../module/_module.php");

/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/

$tp_index	 =	"_tp_detail.html";


/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/

$strLogInBar	=	 CheckLogin();

/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/

$Code	=	$_REQUEST["Code"];


/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/
$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
if (!$conn) {die('Not connected : ' . mysql_error());}
// make foo the current db
$db_selected = mysql_select_db($strCfgDbName, $conn);
if (!$db_selected) { die ('Can\'t use $strCfgDbName : ' . mysql_error());}
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");
$tp = new Template($tp_index);

/*---------- Menu --------------*/
$sql	=	"SELECT `SubCateId`,`SubCateName` FROM $strCfgDbTableSubCategory WHERE `CateId`='1' AND (IsDelete='N' AND Active='Y') ORDER BY `LineNo`,`SubCateId` ASC";
$result	=	mysql_query($sql);
$tp->block("ListCate");
while($row=mysql_fetch_assoc($result)){
	$SubCateId		= $row[SubCateId];
	$SubCateName	=	$row[SubCateName];
	$tp->apply();
}
/*----------// Menu --------------*/

/*--------- Product ---------------*/
$sql="SELECT `ProductId`,`ProductCode`,`ProductName`,`SubCateId`,`Price`,`AfterDisPrice`,`Detail`,`ColorId`,`Size` AS ProductSize,`File1`,`File2`,`File3`,`File4` FROM $strCfgDbTableProduct WHERE `ProductCode`='$Code' AND (IsDelete='N' AND `Active`='Y')  ORDER BY `LineNo` ASC,`ProductId` DESC Limit 0,1";
$result	=	mysql_query($sql);
@extract(mysql_fetch_array($result));
if($AfterDisPrice!="0.00")
{
	$Price="<span style=\"color:#ababab;text-decoration:line-through;\"  class=\"text_13b_1\">".$Price." THB</span> &nbsp;<b>$AfterDisPrice</b>";
}else{
	$AfterDisPrice=$Price;
}
$PathFile1	=$strCfgWebPathProduct."/".$File1;
$PathFile2	=$strCfgWebPathProduct."/".$File2;
$PathFile3	=$strCfgWebPathProduct."/".$File3;
$PathFile4	=$strCfgWebPathProduct."/".$File4;

$SmallImg1	=	"../module/phpThumb/phpThumb.php?src=../../content/product/$File1&w=$strCfgProductSmallThumbWidth&h=$strCfgProductSmallThumbHeight&zc=1&sx=0";
$SmallImg2	=	"../module/phpThumb/phpThumb.php?src=../../content/product/$File2&w=$strCfgProductSmallThumbWidth&h=$strCfgProductSmallThumbHeight&zc=1&sx=0";
$SmallImg3	=	"../module/phpThumb/phpThumb.php?src=../../content/product/$File3&w=$strCfgProductSmallThumbWidth&h=$strCfgProductSmallThumbHeight&zc=1&sx=0";
$SmallImg4	=	"../module/phpThumb/phpThumb.php?src=../../content/product/$File4&w=$strCfgProductSmallThumbWidth&h=$strCfgProductSmallThumbHeight&zc=1&sx=0";

$BigImg=$PathFile1;

/*---------// Product ---------------*/

/*---------Relate Product ---------------*/
$sql="SELECT `ProductId`,`ProductCode`,`ProductName`,`File1` FROM $strCfgDbTableProduct WHERE `ProductCode`='$Code' AND (IsDelete='N' AND `Active`='Y')  ORDER BY `LineNo` ASC,`ProductId` DESC";
$result	=	mysql_query($sql);
$tp->block("ListRelate");
while($row=mysql_fetch_assoc($result)){
	$RelateId		=	$row["ProductId"];
	$RelateName	=	$row["ProductName"];
	$RelateFile1			=	$row["File1"];
	$RelateImg	=	"../module/phpThumb/phpThumb.php?src=../../content/product/$RelateFile1&w=$strCfgProductSmallThumbWidth&h=$strCfgProductSmallThumbHeight&zc=1&sx=0";
	$tp->apply();
}


/*---------//Relate Product ---------------*/

/*---------- Size --------------*/
$arr_product_size	= explode("|",$ProductSize);
$sql="SELECT `SizeId`,`Size` FROM $strCfgDbTableSize WHERE IsDelete='N' AND `Active`='Y' ORDER BY  LineNo ASC,SizeId DESC";
$result	= mysql_query($sql);
$tp->block("ListSize");
$CntSize=0;
while($row=mysql_fetch_assoc($result)){
	$BoxStyle	="";
	$SizeId		= $row[SizeId];
	$Size	=	$row[Size];
	if(in_array($SizeId,$arr_product_size)){
		$BoxStyle	="<div class=\"block_size\" id=\"Size".$SizeId."\">$Size</div>";
	}else{
		$BoxStyle	="<div class=\"block_size_disable\"  id=\"Size".$SizeId."\">$Size</div>";
	}
	$tp->apply();
	$CntSize++;
}
/*----------// Size --------------*/

//- QTY
$tp->block("ListQty");
for($i=1;$i<=20;$i++){
	$tp->apply();
}

//-//QTY

$tp->Display();
mysql_close($conn);
exit;


/*------------------------------------------------------------------*/
?>
