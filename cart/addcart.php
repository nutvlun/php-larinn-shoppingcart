<?
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*

Program     : 
Description : 
Programmer  : ตรินัยน์ จันทร์คง

Individual
Email       : thrinai@hotmail.com
Website     : 


Office
Email       : thrinai@digithais.com
Website     : http://www.digithais.com

Date        : 02-07-2009
Modify log  : 

*/
/*------------------------------------------------------------------*/
/*- Include Library --------------------------------------------------------*/
session_start();
include("../module/_config.php");


/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/





/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/

$Today	 =	 date("Y-m-d");


/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/
$Send			=	$_REQUEST["Send"];
$ProductId		=	$_REQUEST["ProductId"];
$ProductName	=	$_REQUEST["ProductName"];
$Price			=	$_REQUEST["Price"];
$Code			=	$_REQUEST["Code"];
$ColorId		=	$_REQUEST["ColorId"];
$Size			=	$_REQUEST["Size"];
$Qty			=	$_REQUEST["Qty"];
$File1			=	$_REQUEST["File1"];



/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/

if($Send=="Y"){
	if(in_array($ProductId,$_SESSION["s_ProductId"])&& in_array($Size,$_SESSION["s_Size"])){
		//print_r($_SESSION);
		header("Location:index.php");
		exit;
	}
		$_SESSION["s_ProductId"][]	=	$ProductId;
		$_SESSION["s_ProductName"][]	=	$ProductName;
		$_SESSION["s_Price"][]		=	$Price;
		$_SESSION["s_Code"][]			=	$Code;
		$_SESSION["s_ColorId"][]		=	$ColorId;
		$_SESSION["s_Size"][]			=	$Size;
		$_SESSION["s_Qty"][]			=	$Qty;
		$_SESSION["s_File1"][]		=	$File1;
		$_SESSION["s_inCart"]		=	Count($_SESSION["s_ProductId"]);

}
//print_r($_SESSION);
header("Location:index.php");
exit;

/*- //Program --------------------------------------------------------*/
?>