<?
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*

Program     : 
Description : 
Programmer  : ตรินัยน์ จันทร์คง

Individual
Email       : thrinai@hotmail.com
Website     : 


Office
Email       : thrinai@digithais.com
Website     : http://www.digithais.com

Date        : 02-07-2009
Modify log  : 

*/
/*------------------------------------------------------------------*/
/*- Include Library --------------------------------------------------------*/

include("../module/SiXhEaD.Template.php");
include("../module/SiXhEaD.Pagination.php");
include("../module/_config.php");
include("../module/_module.php");
/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/


$tp_index	=	"_tp_index.html";


/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/

$Today	 =	 date("Y-m-d");
$strLogInBar	=	 CheckLogin();

/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/

$action	=	$_REQUEST["action"];


/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/

if($Send=="Y"){
	for($i=0;$i<intval($_SESSION["s_inCart"]);$i++){
		$pid	=	$i+1;
		$_SESSION["s_Qty"][$i]	=	$_REQUEST["total_units_".$pid];
	}
		
	header("Location:index.php");
}

$tp = new Template($tp_index);
$tp->block("data");
$total_price_sum_t	=	0;
for($i=0;$i<intval($_SESSION["s_inCart"]);$i++){
	$ProductId		=	$_SESSION["s_ProductId"][$i];
	$title			=	$_SESSION["s_ProductName"][$i];
	$price_t		=	$_SESSION["s_Price"][$i];
	$code 			=	$_SESSION["s_Code"][$i];
	$ColorId		=	$_SESSION["s_ColorId"][$i];
	$Size			=	$_SESSION["s_Size"][$i];
	$total_unit		=	$_SESSION["s_Qty"][$i];
	$File1			=	$_SESSION["s_File1"][$i];
	$Img=	"../module/phpThumb/phpThumb.php?src=../../content/product/$File1&w=$strCfgProductThumbWidth&h=$strCfgProductThumbHeight&zc=1&sx=0";
	$total_price	=	$total_unit*$price_t;
	$total_price_t	=	number_format($total_price,2, '.', ',');
	$total_price_sum_t	+=$total_price;
	$pid	=	$i+1;
	$tp->apply();
}

$total_price_sum_t	=	number_format($total_price_sum_t,2, '.', ',');
if($action!="checkout"){
	$tp->block("btn");
	$tp->apply();
}
$tp->Display();
exit;

/*- //Program --------------------------------------------------------*/
?>