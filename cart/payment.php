<?
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*

Program     : 
Description : 
Programmer  : ตรินัยน์ จันทร์คง

Individual
Email       : thrinai@hotmail.com
Website     : 


Office
Email       : thrinai@digithais.com
Website     : http://www.digithais.com

Date        : 02-07-2009
Modify log  : 

*/
/*------------------------------------------------------------------*/
/*- Include Library --------------------------------------------------------*/

include("../module/SiXhEaD.Template.php");
include("../module/SiXhEaD.Pagination.php");
include("../module/_config.php");
include("../module/_module.php");
/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/


$tp_index	=	"_tp_payment.html";


/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/

authen_user ();
$strLogInBar	=	 CheckLogin();

/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/

$payment_method	=	$_REQUEST["payment_method"];
$su_member_id	=	$_SESSION["su_member_id"];


/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/
if ($payment_method) { 
	$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
	if (!$conn) {die('Not connected : ' . mysql_error());}
	// make foo the current db
	$db_selected = mysql_select_db($strCfgDbName, $conn);
	if (!$db_selected) { die ('Can\'t use $strCfgDbName : ' . mysql_error());}
	mysql_query("SET NAMES UTF8");
	mysql_query("SET character_set_results=UTF8");
	if(intval($_SESSION["s_inCart"])>0){
		$total_price_sum_t	=	0;
		$sql	=	"INSERT INTO $strCfgDbTableInvH(`IsDelete`,`MemberId`,`Total`,`PayBy`,`Status`,`AddDate`)VALUES('N','$su_member_id','0','$payment_method','wait_pay',Now())";
		mysql_query($sql);
		//echo "$sql";
		$InvId	=	mysql_insert_id();
		$InvNo	=	substr("0000000".$InvId,-7);
		$sql="INSERT INTO $strCfgDbTableInvDtl(`IsDelete`,`InvHId`,`ProductId`,`ProductCode`,`ProductName`,`Size`,`ColorId`,`Qty`,`UnitPrice`,`SubTotal`,`Active`,`AddDate`)VALUES";
		for($i=0;$i<intval($_SESSION["s_inCart"]);$i++){
			$ProductId		=	$_SESSION["s_ProductId"][$i];
			$ProductName	=	$_SESSION["s_ProductName"][$i];
			$price_t		=	$_SESSION["s_Price"][$i];
			$code 			=	$_SESSION["s_Code"][$i];
			$ColorId		=	$_SESSION["s_ColorId"][$i];
			$Size			=	$_SESSION["s_Size"][$i];
			$total_unit		=	$_SESSION["s_Qty"][$i];	
			$total_price	=	$total_unit*$price_t;
			$total_price_t	=	number_format($total_price,2, '.', ',');
			$total_price_sum_t	+=$total_price;
			
			if($i>0)$sql.=",";
			$sql.="('N','$InvId','$ProductId','$code','$ProductName','$Size','$ColorId','$total_unit','$price_t','$total_price','Y',Now())";
			
	
		}
		//echo "$sql";
		mysql_query($sql);
		$sql="UPDATE $strCfgDbTableInvH SET InvNo='$InvNo',`Total`='$total_price_sum_t' WHERE InvHId='$InvId'";
		mysql_query($sql);
	}
	
	mysql_close($conn);
	unset($_SESSION["s_ProductId"]);
	unset($_SESSION["s_ProductName"]);
	unset($_SESSION["s_Price"]);
	unset($_SESSION["s_Code"]);
	unset($_SESSION["s_ColorId"]);
	unset($_SESSION["s_Size"]);
	unset($_SESSION["s_Qty"]);
	unset($_SESSION["s_File1"]);
	$_SESSION["s_inCart"]=0;
	header("Location:finish.php?InvId=$InvId");
	exit;
}
$tp = new Template($tp_index);

$tp->Display();
exit;

/*- //Program --------------------------------------------------------*/
?>