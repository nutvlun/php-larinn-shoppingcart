<?
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*

Program     : 
Description : 
Programmer  : ตรินัยน์ จันทร์คง

Individual
Email       : thrinai@hotmail.com
Website     : 


Office
Email       : thrinai@digithais.com
Website     : http://www.digithais.com

Date        : 02-07-2009
Modify log  : 

*/
/*------------------------------------------------------------------*/
/*- Include Library --------------------------------------------------------*/
session_start();
include("../module/_config.php");


/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/





/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/

$Today	 =	 date("Y-m-d");


/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/
$pid			=	$_REQUEST["pid"];




/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/
$pid	=	$pid-1;

unset($_SESSION["s_ProductId"][$pid]);
unset($_SESSION["s_ProductName"][$pid]);
unset($_SESSION["s_Price"][$pid]);
unset($_SESSION["s_Code"][$pid]);
unset($_SESSION["s_ColorId"][$pid]);
unset($_SESSION["s_Size"][$pid]);
unset($_SESSION["s_Qty"][$pid]);
unset($_SESSION["s_File1"][$pid]);

$_SESSION["s_ProductId"]=array_values($_SESSION["s_ProductId"]);
$_SESSION["s_ProductName"]=array_values($_SESSION["s_ProductName"]);
$_SESSION["s_Price"]=array_values($_SESSION["s_Price"]);
$_SESSION["s_Code"]=array_values($_SESSION["s_Code"]);
$_SESSION["s_ColorId"]=array_values($_SESSION["s_ColorId"]);
$_SESSION["s_Size"]=array_values($_SESSION["s_Size"]);
$_SESSION["s_Qty"]=array_values($_SESSION["s_Qty"]);
$_SESSION["s_File1"]=array_values($_SESSION["s_File1"]);
$_SESSION["s_inCart"]		=	Count($_SESSION["s_ProductId"]);


header("Location:index.php");
exit;

/*- //Program --------------------------------------------------------*/
?>