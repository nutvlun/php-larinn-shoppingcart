<?
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*

Program     : 
Description : 
Programmer  : ตรินัยน์ จันทร์คง

Individual
Email       : thrinai@hotmail.com
Website     : 


Office
Email       : thrinai@digithais.com
Website     : http://www.digithais.com

Date        : 02-07-2009
Modify log  : 

*/
/*------------------------------------------------------------------*/
/*- Include Library --------------------------------------------------------*/

include("../module/SiXhEaD.Template.php");
include("../module/SiXhEaD.Pagination.php");
include("../module/_config.php");
include("../module/_module.php");
/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/


$tp_index			=	"_tp_index.html";


/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/

$Today	 =	 date("Y-m-d");
$strLogInBar	=	 CheckLogin();

/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/


$searchq		=	$_REQUEST[searchq];
$order_by		=	$_REQUEST[order_by];
$order			=	$_REQUEST[order];
$LimitPage		=	$_REQUEST[LimitPage];
$page			=	($_REQUEST[page]=="")?1:$_REQUEST[page];

/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/

$tp = new Template($tp_index);

$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
if (!$conn) {die('Not connected : ' . mysql_error());}
// make foo the current db
$db_selected = mysql_select_db($strCfgDbName, $conn);
if (!$db_selected) { die ('Can\'t use $strCfgDbName : ' . mysql_error());}
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");
//- จัด Range ใหม่

if(!$LimitPage){$LimitPage=10;}
//- Condition
$WHERE = " ";
$order_by = " Mark DESC,LineNo ASC,`NewsId` DESC ";
//$WHERE	=	" AND `NewsType`='$cate' AND  ActivityDate<='$Today'";
$WHERE	=	" AND  ActivityDate<='$Today'";


$sp	= New Pagination();
$sp->db_type			=	"MySQL";
$sp->db_table			=	$strCfgDbTableNews;
$sp->order_by			=	$order_by; 
$sp->primary_key	=	"`NewsId`";
$sp->select			=	"`NewsId`,`LineNo`,`Mark`,`ThTitle`,`ThShortDetail`,`File1`,ActivityDate";
$sp->custom_sql		=	" WHERE  IsDelete='N' And `Active`='Y' $WHERE  " ; # WHERE, JOIN
$sp->custom_param	=	"&LimitPage=$LimitPage&cate=$cate&searchq=$searchq";
$sp->per_page		=	$LimitPage;
$sp->order_param	=	"order"; # $_GET["order"];
$sp->page_param	=	"page"; # $_REQUEST["p"];
$sp->page_prev		=	"&#8249; Prev"; //<span class="yellow12" style="font-weight: bold">1</span> | <a href="#" class="white12-a">2</a> | 3 | 4 | 5
$sp->page_next		=	"Next &#8250;";
$sp->page_first		=	"&laquo; First";
$sp->page_last		=	"Last &raquo;";
$sp->sign_left			=	"";
$sp->sign_right		=	" |";
$sp->sign_trim		=	FALSE;
$sp->style				=	1;
$sp->start();

$intRecordAll	=	$sp->get_record_all();
$sql				=	$sp->get_sql();
$strPageLink	=	$sp->get_page_link();
$intPageAll		=	$sp->get_all_page();

$bg_color = "";
$result = mysql_query($sql); //echo $sql;
if (!$result) { echo "$sql"; die('Invalid query: ' . mysql_error()); }
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");


$cols	=	3;
$tp->block("DATA");
$tp->sub($cols);
$i=0;
$strTbl	=	"";
while($row = mysql_fetch_array($result)) {
	$Id					=	$row["NewsId"];
	$TblID = <<<Data
ID="$NewsId";
Data;
	$LineNo				=	$row["LineNo"];
	$Mark					=	$row["Mark"];
	$Title					=	$row["ThTitle"];
	$ShortDetail			=	$row["ThShortDetail"];
	$File1					=	$row["File1"];
	
	$ImgNews = <<<img
../module/phpThumb/phpThumb.php?src=../../content/news/$File1&w=$strCfgNewsThumbWidth&h=$strCfgNewsThumbHeight&zc=1&sx=0
img;
	
	$tp->apply();
	$i++;
}
if(intval($i%$cols)>0){
	$intBlank	 =	($cols-intval($i%$cols));
	for($j=0;$j<$intBlank;$j++){
		$strTbl	.=	<<<strdata
			<td width="333" height="320" align="center" valign="top"><img src="../images/Share/spacer.gif" width="290" height="7" /></td>
strdata;
	}
	$strTbl	.=	<<<strdata

                                      </tr>
                                    </table>
									</td>
                                </tr>
strdata;
}
mysql_free_result($result);
mysql_close($conn);



$tp->Display();
exit;

?>