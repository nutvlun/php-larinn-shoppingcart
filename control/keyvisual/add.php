<?
/*------------------------------------------------------------------*/
/*- Require --------------------------------------------------------*/

include("../../module/_config.php");
include("../../module/SiXhEaD.Template.php");
include("../../module/SiXhEaD.Pagination.php");
include("../../module/_module.php");
include("../../module/_module_control.php");

/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/

$tp_add			=	"_tp_add.html";
$tp_complete	=	"_tp_complete.html";
$tp_error			=	"_tp_error.html";

/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/

$DefineWidth	=	$strCfgKeyVisualWidth	;
$DefineHeight	=	$strCfgKeyVisualHeight;



/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/
$Send			=	$_REQUEST['Send'];
$Id				=	$_REQUEST['Id'];
$Label	=	htmlspecialchars($_REQUEST['Label'], ENT_QUOTES);
$File1NameBak		=	$_REQUEST['File1NameBak'];
$Url	=	$_REQUEST['Url'];
if($_REQUEST['Active']){$Active ="Y";}else{$Active ="N";}




/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/
	$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
	if (!$conn) {die('Not connected : ' . mysql_error());}
	// make foo the current db
	$db_selected = mysql_select_db($strCfgDbName, $conn);
	if (!$db_selected) { die ('Can\'t use $strCfgDbName : ' . mysql_error());}
	mysql_query("SET NAMES UTF8");
	mysql_query("SET character_set_results=UTF8");

if (!$Send) { 
	$ActiveChecked						=	"checked";

	$tp = new Template($tp_add);
	if($Id){
		$sql="SELECT * FROM $strCfgDbTableKeyvisual WHERE KeyId='$Id'";
		$result=mysql_query($sql);
		@extract(mysql_fetch_array($result,MYSQL_ASSOC));
		if($Active=="N") $ActiveChecked="";
		$Photo1ViewAndDeleteLink="<br><img src=\"$strCfgWebPathKeyvisual/$File1\" border=\"0\" ID=\"IMG1\" width=\"150\"><br><a 
		href=\"javascript:confirmBox('$strCfgWebPathKeyvisual/$File1');\" class=\"LinkCancel\">[View]</a>&nbsp;<a href=\"javascript:confirmBox('del_file.php?Field=File1&Id=$Id');\" 
		class=\"LinkCancel\">[Delete]</a>";
		mysql_free_result($result);
		mysql_close($conn);
	}
	
	$tp->Display();
	
	exit;	

} else {
	
	
	if($Id){
			$strFileField			=	"File1";
			$NewFileName			=	"$Id-File1";
			$strUploadToPath		=	"$strCfgUploadPathKeyvisual";
			$strOnlyFileType		=	"image"; # File type header. Use | for more type eg. txt|pdf|doc 
			$intMaxFileSize			=	2097152; # 2M
			if ($_FILES["$strFileField"]["tmp_name"]) {
				list($owidth, $oheight)	= getimagesize($_FILES["$strFileField"]["tmp_name"]);
				
				
				$strUploadFileArr	=	upload_file($strFileField,$NewFileName,$strUploadToPath,$strOnlyFileType,$intMaxFileSize);
				$NewFileName=$strUploadFileArr[name];
				if($owidth>$DefineWidth){
						resizeImageUpload($strCfgUploadPathKeyvisual,$NewFileName,$strCfgUploadPathNews,$NewFileName,$DefineWidth);
				}
				$sql_update	.=	",`File1`='".$NewFileName."' ";
			}
		$sql="UPDATE $strCfgDbTableKeyvisual SET `Label`='$Label',Url='$Url',`Active`='$Active'".$sql_update." WHERE `KeyId`='$Id'";
		mysql_query($sql);

	}else{
		$sql="INSERT INTO $strCfgDbTableKeyvisual(`IsDelete`,`Label`,`Url`,`Active`,`AddDate`)VALUES('N','$Label','$Url','N',Now())";
		mysql_query($sql);
		$Id = mysql_insert_id();
		$strFileField			=	"File1";
		$NewFileName			=	"$Id-File1";
		$strUploadToPath		=	"$strCfgUploadPathKeyvisual";
		$strOnlyFileType		=	"image"; # File type header. Use | for more type eg. txt|pdf|doc 
		$intMaxFileSize			=	2097152; # 2M
		if ($_FILES["$strFileField"]["tmp_name"]) {
			list($owidth, $oheight)	= getimagesize($_FILES["$strFileField"]["tmp_name"]);
				
				
			$strUploadFileArr	=	upload_file($strFileField,$NewFileName,$strUploadToPath,$strOnlyFileType,$intMaxFileSize);
			$NewFileName=$strUploadFileArr[name];
			if($owidth>$DefineWidth){
					resizeImageUpload($strCfgUploadPathKeyvisual,$NewFileName,$strCfgUploadPathNews,$NewFileName,$DefineWidth);
			}
			$sql="UPDATE $strCfgDbTableKeyvisual SET `File1`='".$NewFileName."',`Active`='$Active' WHERE `KeyId`='$Id'";
			mysql_query($sql);
		}
		
	}
	
	mysql_close($conn);
	$tp = new Template($tp_complete);
	$tp->Display();
	
	exit;
}

/*------------------------------------------------------------------*/
?>