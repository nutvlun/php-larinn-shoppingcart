<?
/*------------------------------------------------------------------*/
/*- Require --------------------------------------------------------*/

include("../../module/_config.php");
include("../../module/SiXhEaD.Template.php");
include("../../module/SiXhEaD.Pagination.php");
include("../../module/_module.php");
include("../../module/_module_control.php");


/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/

$tp_index		=	"_tp_index.html";

/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/


/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/

$Send			=	$_REQUEST['Send'];
$range			=	$_REQUEST['range'];
$searchq		=	$_REQUEST['searchq'];
$order_by		=	$_REQUEST['order_by'];
$order			=	$_REQUEST['order'];
$LimitPage		=	$_REQUEST['LimitPage'];
$page			=	$_REQUEST['page'];
$cate			=	$_REQUEST['cate'];


/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/
$tp = new Template($tp_index);
$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
if (!$conn) {die('Not connected : ' . mysql_error());}
// make foo the current db
$db_selected = mysql_select_db($strCfgDbName, $conn);
if (!$db_selected) { die ('Can\'t use $strCfgDbName : ' . mysql_error());}
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");
//- จัด Range ใหม่

if($Send=="Y"){

	if($range!=""){
			

		$arr_range=explode(",",$range);
		$k=1;
		for($i=0;$i<count($arr_range);$i++){
			if(intval($arr_range[$i])>0){
		
				$Id=intval($arr_range[$i]);
				$sql="UPDATE $strCfgDbTableProduct SET LineNo='$k' WHERE `ProductId`='$Id'";
				mysql_query($sql);
				$k++;
			}

		}

	}
}

/*-----------------------------------------------------------------*/


if(!$LimitPage){$LimitPage=30;}
//- Category
$sql="SELECT `SubCateId`,`SubCateName` FROM $strCfgDbTableSubCategory WHERE `CateId`='1' AND(IsDelete='N' AND Active='Y')  ORDER BY LineNo ASC,`SubCateId` DESC";
$result	=	mysql_query($sql);
$tp->block("ListCate");
while($row = mysql_fetch_array($result)){
	$SubCate_Id	=	$row['SubCateId'];
	$SubCate_Name	=	$row['SubCateName'];
	$tp->apply();
}


//- Condition
$WHERE = " ";
if($cate)$WHERE	.=	"AND $strCfgDbTableSubCategory.`SubCateId`='$cate' ";


if($searchq){$WHERE	.=	"AND ( $strCfgDbTableProduct.`ProductCode`  LIKE '%$searchq%' ) OR ( $strCfgDbTableProduct.`ProductName`  LIKE '%$searchq%' ) ";}

//- Order By
$order_by = " $strCfgDbTableProduct.LineNo ASC,$strCfgDbTableProduct.`ProductId` DESC ";


$sp	= New Pagination();
$sp->db_type			=	"MySQL";
$sp->db_table			=	$strCfgDbTableProduct;
$sp->order_by			=	$order_by;
$sp->primary_key	=	"`ProductId`";
$sp->select			=	"$strCfgDbTableProduct.`ProductId` As `ProductId`,$strCfgDbTableSubCategory.`SubCateName` As `SubCateName`,$strCfgDbTableProduct.`LineNo` As `LineNo`,$strCfgDbTableProduct.`ProductCode` As `Code`,$strCfgDbTableProduct.`ProductName` As `ProductName`,$strCfgDbTableColor.`ColorName` AS `ColorName`,$strCfgDbTableColor.`ColorCode` `ColorCode`,$strCfgDbTableProduct.`Active` As `Active`,$strCfgDbTableProduct.`Price` As `Price`,$strCfgDbTableProduct.`AfterDisPrice` As `AfterDisPrice`,$strCfgDbTableProduct.`AddDate` As `AddDate`,$strCfgDbTableProduct.`UpdateDate` As `UpdateDate`";
$sp->custom_sql		=	" INNER JOIN $strCfgDbTableSubCategory ON $strCfgDbTableSubCategory.`SubCateId`=$strCfgDbTableProduct.`SubCateId` INNER JOIN $strCfgDbTableColor ON $strCfgDbTableColor.`ColorId`=$strCfgDbTableProduct.`ColorId` WHERE $strCfgDbTableProduct.IsDelete='N'  $WHERE " ; # WHERE, JOIN
$sp->custom_param	=	"&LimitPage=$LimitPage&cate=$cate&searchq=$searchq";
$sp->per_page		=	$LimitPage;
$sp->order_param	=	"order"; # $_GET["order"];
$sp->page_param	=	"page"; # $_REQUEST["p"];
$sp->page_prev		=	"&#8249; Prev";
$sp->page_next		=	"Next &#8250;";
$sp->page_first		=	"&laquo; หน้าแรก";
$sp->page_last		=	"หน้าสุดท้าย &raquo;";
$sp->sign_left			=	"";
$sp->sign_right		=	" |";
$sp->sign_trim		=	FALSE;
$sp->style				=	1;
$sp->start();

$intRecordAll		=	$sp->get_record_all();
$sql				=	$sp->get_sql();
$strPageLink		=	$sp->get_page_link();

$bg_color = "";
$result = mysql_query($sql); //echo $sql;
if (!$result) { echo "$sql"; die('Invalid query: ' . mysql_error()); }
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");

$tp->block("DATA");
$i=0;
while($row = mysql_fetch_array($result)) {
	$Id				=	$row[ProductId];
	$TblID = <<<Data
ID="$Id";
Data;
	$LineNo				=	$row['LineNo'];
	$SubCateName		=	$row['SubCateName'];
	$Code				=	$row['Code'];
	$ProductName		=	$row['ProductName'];
	$ColorName			=	$row['ColorName'];
	$ColorCode			=	$row['ColorCode'];
	$Active				=	$row['Active'];
	$Price				=	$row['Price'];
	$AfterDisPrice		=	$row['AfterDisPrice'];
	if($AfterDisPrice!="0.00")$Price="<span style=\"text-decoration:line-through;\">".$Price."</span>&nbsp;<b>$AfterDisPrice</b>";
	$AddDate			=	YYYYMMDDHHMMSS2DDMMYYYYHHMM($row['AddDate']);
	$UpdateDate			=	YYYYMMDDHHMMSS2DDMMYYYYHHMM($row['UpdateDate']);
	
	// - Active 
	if ($Active == "Y") {
		$strActiveNew	=	"N";
		$strActive		=	"$Active";
		$strActiveLinkCss=	"LinkYes";
	}
	else {
		$strActiveNew	=	"Y";
		$strActive		=	"$Active";
		$strActiveLinkCss=	"LinkNo";
	}
	$strBgColor = "#FFFFFF";
	$strBgColorOver = "#D9FFD9";

	

	//-Color Row
	
	$strCssTr = " bgcolor=\"$strBgColor\" onmouseover=\"this.style.background='$strBgColorOver'\" onmouseout=\"this.style.background='$strBgColor'\" ";
	$tp->apply();
	$i++;
}
mysql_free_result($result);
mysql_close($conn);



$tp->Display();
exit;

/*------------------------------------------------------------------*/
?>