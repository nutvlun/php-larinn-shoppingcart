<?
/*------------------------------------------------------------------*/
/*- Require --------------------------------------------------------*/

include("../../module/_config.php");
include("../../module/SiXhEaD.Template.php");
include("../../module/SiXhEaD.Pagination.php");
include("../../module/_module.php");
include("../../module/_module_control.php");

/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/

$tp_add			=	"_tp_add.html";
$tp_complete	=	"_tp_complete.html";
$tp_error			=	"_tp_error.html";

/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/

$DefineWidth	=	$strCfgProductWidth;
$DefineHeight	=	$strCfgProductHeight;


/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/
$Send			=	$_REQUEST['Send'];
$Id				=	$_REQUEST['Id'];
$SubCateId		=	$_REQUEST['SubCateId'];
$ProductCode	=	$_REQUEST['ProductCode'];
$ProductName	=	htmlspecialchars($_REQUEST['ProductName'], ENT_QUOTES);
$ColorId		=	$_REQUEST['ColorId'];
$Detail			=	$_REQUEST['Detail'];
$Price			=	$_REQUEST['Price'];
$AfterDisPrice	=	$_REQUEST['AfterDisPrice'];
$CountSize		=	$_REQUEST['CountSize'];
if($_REQUEST['Active']){$Active ="Y";}else{$Active ="N";}

/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/
	$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
	if (!$conn) {die('Not connected : ' . mysql_error());}
	// make foo the current db
	$db_selected = mysql_select_db($strCfgDbName, $conn);
	if (!$db_selected) { die ('Can\'t use $strCfgDbName : ' . mysql_error());}
	mysql_query("SET NAMES UTF8");
	mysql_query("SET character_set_results=UTF8");

if (!$Send) { 
	$ActiveChecked						=	"checked";
	
	$tp = new Template($tp_add);

	if($Id){
		$sql="SELECT * FROM $strCfgDbTableProduct WHERE ProductId='$Id'";
		$result=mysql_query($sql);
		@extract(mysql_fetch_array($result,MYSQL_ASSOC));
		mysql_free_result($result);
		$Photo1ViewAndDeleteLink="<br><img src=\"$strCfgWebPathProduct/$File1\" border=\"0\" ID=\"IMG1\" width=\"150\"><br><a 
		href=\"javascript:confirmBox('$strCfgWebPathProduct/$File1');\" class=\"LinkCancel\">[View]</a>&nbsp;<a href=\"javascript:confirmBox('del_file.php?Field=File1&Id=$Id');\" 
		class=\"LinkCancel\">[Delete]</a>";
		$Photo2ViewAndDeleteLink="<br><img src=\"$strCfgWebPathProduct/$File2\" border=\"0\" ID=\"IMG2\" width=\"150\"><br><a 
		href=\"javascript:confirmBox('$strCfgWebPathProduct/$File2');\" class=\"LinkCancel\">[View]</a>&nbsp;<a href=\"javascript:confirmBox('del_file.php?Field=File2&Id=$Id');\" 
		class=\"LinkCancel\">[Delete]</a>";
		$Photo3ViewAndDeleteLink="<br><img src=\"$strCfgWebPathProduct/$File3\" border=\"0\" ID=\"IMG3\" width=\"150\"><br><a 
		href=\"javascript:confirmBox('$strCfgWebPathProduct/$File3');\" class=\"LinkCancel\">[View]</a>&nbsp;<a href=\"javascript:confirmBox('del_file.php?Field=File3&Id=$Id');\" 
		class=\"LinkCancel\">[Delete]</a>";
		$Photo4ViewAndDeleteLink="<br><img src=\"$strCfgWebPathProduct/$File4\" border=\"0\" ID=\"IMG4\" width=\"150\"><br><a 
		href=\"javascript:confirmBox('$strCfgWebPathProduct/$File4');\" class=\"LinkCancel\">[View]</a>&nbsp;<a href=\"javascript:confirmBox('del_file.php?Field=File4&Id=$Id');\" 
		class=\"LinkCancel\">[Delete]</a>";
		
	}
	$sql="SELECT `SubCateName`,`SubCateId` FROM $strCfgDbTableSubCategory WHERE `CateId`=1 AND(IsDelete='N' AND Active='Y')  ORDER BY LineNo ASC,`SubCateId` DESC";
	$result	=	mysql_query($sql);
	$tp->block("ListCate");
	while($row = mysql_fetch_assoc($result)){
		$CateSelected	=	" ";
		$CateId			=	$row['SubCateId'];
		$SubCateName	=	$row['SubCateName'];
		if($CateId==$SubCateId) $CateSelected	=	" selected";
		$tp->apply();
	}
	$sql="SELECT `ColorId`,`ColorCode`,`ColorName` FROM $strCfgDbTableColor WHERE (IsDelete='N' AND Active='Y')  ORDER BY LineNo ASC,`ColorName` ASC";
	$result	=	mysql_query($sql);
	$tp->block("ListColor");
	while($row = mysql_fetch_assoc($result)){
		$ColorSelected	=	" ";
		$Color			=	$row['ColorId'];
		$ColorName	=	$row['ColorName'];
		if($ColorId==$Color) $ColorSelected	=	" selected";
		$tp->apply();
	}
	$sql="SELECT `SizeId`,`Size` FROM $strCfgDbTableSize WHERE (IsDelete='N' AND Active='Y')  ORDER BY LineNo ASC,`Size` ASC";
	$result	=	mysql_query($sql);
	$tp->block("ListSize");
	$arr_Size	=	explode("|",$Size);
	$cntSize	=	0;
	while($row = mysql_fetch_assoc($result)){
		$i	=	$cntSize+1;
		$SizeSelected	=	" ";
		$SizeId			=	$row['SizeId'];
		$SizeTxt		=	$row['Size'];
		if (in_array($SizeId, $arr_Size)) {
			$SizeSelected	=	" checked";
		}

		$cntSize++;
		$tp->apply();
	}
	
	$tp->Display();
	mysql_close($conn);
	exit;	

} else {
	$Size="";
	$k=0;
	for($i=1;$i<($CountSize+1);$i++){
		$SizeId=$_REQUEST['Size'.$i];
		if($SizeId){
			if($k>0)$Size.="|";
			$Size.=$SizeId;
			$k++;
		}
	}
	//echo "$Size";
//exit;

	if($Id){
		// - Upload  Image
		// - Upload File 1
		$strFileField			=	"File1";
		$NewFileName			=	"$Id-File1";
		$strUploadToPath		=	"$strCfgUploadPathProduct";
		$strOnlyFileType		=	"image"; # File type header. Use | for more type eg. txt|pdf|doc 
		$intMaxFileSize			=	2097152; # 2M
		if ($_FILES["$strFileField"]["tmp_name"]) {
			list($owidth, $oheight)	= getimagesize($_FILES["$strFileField"]["tmp_name"]);
			$strUploadFileArr	=	upload_file($strFileField,$NewFileName,$strUploadToPath,$strOnlyFileType,$intMaxFileSize);
			$NewFileName=$strUploadFileArr[name];
			if($owidth>$DefineWidth){
					resizeImageUpload($strCfgUploadPathNews,$NewFileName,$strCfgUploadPathNews,$NewFileName,$DefineWidth);
			}
			$File_Name1 = $NewFileName;
		}else{
			$NewFileName=$_REQUEST['FileNameBak1'];
			$File_Name1 = $NewFileName;
		}
		// -// Upload File 1
		// - Upload File 2
		$strFileField			=	"File2";
		$NewFileName			=	"$Id-File2";
		$strUploadToPath		=	"$strCfgUploadPathProduct";
		$strOnlyFileType		=	"image"; # File type header. Use | for more type eg. txt|pdf|doc 
		$intMaxFileSize			=	2097152; # 2M
		if ($_FILES["$strFileField"]["tmp_name"]) {
			list($owidth, $oheight)	= getimagesize($_FILES["$strFileField"]["tmp_name"]);
			$strUploadFileArr	=	upload_file($strFileField,$NewFileName,$strUploadToPath,$strOnlyFileType,$intMaxFileSize);
			$NewFileName=$strUploadFileArr[name];
			if($owidth>$DefineWidth){
					resizeImageUpload($strCfgUploadPathNews,$NewFileName,$strCfgUploadPathNews,$NewFileName,$DefineWidth);
			}
			$File_Name2 = $NewFileName;
		}else{
			$NewFileName=$_REQUEST['FileNameBak2'];
			$File_Name2 = $NewFileName;
		}
		// -// Upload File 2
		// - Upload File 3
		$strFileField			=	"File3";
		$NewFileName			=	"$Id-File3";
		$strUploadToPath		=	"$strCfgUploadPathProduct";
		$strOnlyFileType		=	"image"; # File type header. Use | for more type eg. txt|pdf|doc 
		$intMaxFileSize			=	2097152; # 2M
		if ($_FILES["$strFileField"]["tmp_name"]) {
			list($owidth, $oheight)	= getimagesize($_FILES["$strFileField"]["tmp_name"]);
			$strUploadFileArr	=	upload_file($strFileField,$NewFileName,$strUploadToPath,$strOnlyFileType,$intMaxFileSize);
			$NewFileName=$strUploadFileArr[name];
			if($owidth>$DefineWidth){
					resizeImageUpload($strCfgUploadPathNews,$NewFileName,$strCfgUploadPathNews,$NewFileName,$DefineWidth);
			}
			$File_Name3 = $NewFileName;
		}else{
			$NewFileName=$_REQUEST['FileNameBak3'];
			$File_Name3 = $NewFileName;
		}
		// -// Upload File 3
		// - Upload File 4
		$strFileField			=	"File4";
		$NewFileName			=	"$Id-File4";
		$strUploadToPath		=	"$strCfgUploadPathProduct";
		$strOnlyFileType		=	"image"; # File type header. Use | for more type eg. txt|pdf|doc 
		$intMaxFileSize			=	2097152; # 2M
		if ($_FILES["$strFileField"]["tmp_name"]) {
			list($owidth, $oheight)	= getimagesize($_FILES["$strFileField"]["tmp_name"]);
			$strUploadFileArr	=	upload_file($strFileField,$NewFileName,$strUploadToPath,$strOnlyFileType,$intMaxFileSize);
			$NewFileName=$strUploadFileArr[name];
			if($owidth>$DefineWidth){
					resizeImageUpload($strCfgUploadPathNews,$NewFileName,$strCfgUploadPathNews,$NewFileName,$DefineWidth);
			}
			$File_Name4 = $NewFileName;
		}else{
			$NewFileName=$_REQUEST['FileNameBak4'];
			$File_Name4 = $NewFileName;
		}
		// -// Upload File 4

	
			
		$sql="UPDATE $strCfgDbTableProduct SET `SubCateId`='$SubCateId',`ProductCode`='$ProductCode',`ProductName`='$ProductName',`Price`='$Price',`AfterDisPrice`='$AfterDisPrice',`Detail`='$Detail',`ColorId`='$ColorId',`Size`='$Size',`Active`='$Active',`File1`='$File_Name1',`File2`='$File_Name2',`File3`='$File_Name3',`File4`='$File_Name4' WHERE `ProductId`='$Id'";
		mysql_query($sql);
		//echo $sql;
	}else{
	
	

		$sql="INSERT INTO 
		$strCfgDbTableProduct(`IsDelete`,`CateId`,`SubCateId`,`ProductCode`,`ProductName`,`Price`,`AfterDisPrice`,`Detail`,`ColorId`,`Size`,`Active`,`AddDate`)VALUES('N','1','$SubCateId','$ProductCode','$ProductName','$Price','$AfterDisPrice','$Detail','$ColorId','$Size','$Active',Now())";
		$result=mysql_query($sql);
		$Id=mysql_insert_id();
		// - Upload  Image
		// - Upload File 1
		$strFileField			=	"File1";
		$NewFileName			=	"$Id-File1";
		$strUploadToPath		=	"$strCfgUploadPathProduct";
		$strOnlyFileType		=	"image"; # File type header. Use | for more type eg. txt|pdf|doc 
		$intMaxFileSize			=	2097152; # 2M
		if ($_FILES["$strFileField"]["tmp_name"]) {
			list($owidth, $oheight)	= getimagesize($_FILES["$strFileField"]["tmp_name"]);
			$strUploadFileArr	=	upload_file($strFileField,$NewFileName,$strUploadToPath,$strOnlyFileType,$intMaxFileSize);
			$NewFileName=$strUploadFileArr[name];
			if($owidth>$DefineWidth){
					resizeImageUpload($strCfgUploadPathNews,$NewFileName,$strCfgUploadPathNews,$NewFileName,$DefineWidth);
			}
			$File_Name1 = $NewFileName;
		}else{
			$NewFileName=$_REQUEST['FileNameBak1'];
			$File_Name1 = $NewFileName;
		}
		// -// Upload File 1
		// - Upload File 2
		$strFileField			=	"File2";
		$NewFileName			=	"$Id-File2";
		$strUploadToPath		=	"$strCfgUploadPathProduct";
		$strOnlyFileType		=	"image"; # File type header. Use | for more type eg. txt|pdf|doc 
		$intMaxFileSize			=	2097152; # 2M
		if ($_FILES["$strFileField"]["tmp_name"]) {
			list($owidth, $oheight)	= getimagesize($_FILES["$strFileField"]["tmp_name"]);
			$strUploadFileArr	=	upload_file($strFileField,$NewFileName,$strUploadToPath,$strOnlyFileType,$intMaxFileSize);
			$NewFileName=$strUploadFileArr[name];
			if($owidth>$DefineWidth){
					resizeImageUpload($strCfgUploadPathNews,$NewFileName,$strCfgUploadPathNews,$NewFileName,$DefineWidth);
			}
			$File_Name2 = $NewFileName;
		}else{
			$NewFileName=$_REQUEST['FileNameBak2'];
			$File_Name2 = $NewFileName;
		}
		// -// Upload File 2
		// - Upload File 3
		$strFileField			=	"File3";
		$NewFileName			=	"$Id-File3";
		$strUploadToPath		=	"$strCfgUploadPathProduct";
		$strOnlyFileType		=	"image"; # File type header. Use | for more type eg. txt|pdf|doc 
		$intMaxFileSize			=	2097152; # 2M
		if ($_FILES["$strFileField"]["tmp_name"]) {
			list($owidth, $oheight)	= getimagesize($_FILES["$strFileField"]["tmp_name"]);
			$strUploadFileArr	=	upload_file($strFileField,$NewFileName,$strUploadToPath,$strOnlyFileType,$intMaxFileSize);
			$NewFileName=$strUploadFileArr[name];
			if($owidth>$DefineWidth){
					resizeImageUpload($strCfgUploadPathNews,$NewFileName,$strCfgUploadPathNews,$NewFileName,$DefineWidth);
			}
			$File_Name3 = $NewFileName;
		}else{
			$NewFileName=$_REQUEST['FileNameBak3'];
			$File_Name3 = $NewFileName;
		}
		// -// Upload File 3
		// - Upload File 4
		$strFileField			=	"File4";
		$NewFileName			=	"$Id-File4";
		$strUploadToPath		=	"$strCfgUploadPathProduct";
		$strOnlyFileType		=	"image"; # File type header. Use | for more type eg. txt|pdf|doc 
		$intMaxFileSize			=	2097152; # 2M
		if ($_FILES["$strFileField"]["tmp_name"]) {
			list($owidth, $oheight)	= getimagesize($_FILES["$strFileField"]["tmp_name"]);
			$strUploadFileArr	=	upload_file($strFileField,$NewFileName,$strUploadToPath,$strOnlyFileType,$intMaxFileSize);
			$NewFileName=$strUploadFileArr[name];
			if($owidth>$DefineWidth){
					resizeImageUpload($strCfgUploadPathNews,$NewFileName,$strCfgUploadPathNews,$NewFileName,$DefineWidth);
			}
			$File_Name4 = $NewFileName;
		}else{
			$NewFileName=$_REQUEST['FileNameBak4'];
			$File_Name4 = $NewFileName;
		}
		// -// Upload File 4
		
		$sql="UPDATE $strCfgDbTableProduct SET `File1`='$File_Name1',`File2`='$File_Name2',`File3`='$File_Name3',`File4`='$File_Name4',`Active`='$Active' WHERE `ProductId`='$Id'";
		mysql_query($sql);
		
	}
	
	mysql_close($conn);
	$tp = new Template($tp_complete);
	$tp->Display();
	
	exit;
}

/*------------------------------------------------------------------*/
?>