<? ini_set('display_errors', 1);
error_reporting("E_ALL");

/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*

Program     : 
Description : 
Programmer  : 

Individual
Email       : pipo@sixhead.com
Website     : http://www.sixhead.com
            : http://www.todaysoftware.com

Office
Email       : pipo@digithais.com
Website     : http://www.digithais.com

Date        : 
Modify log  : 

*/
/*------------------------------------------------------------------*/
/*- Require --------------------------------------------------------*/

include("../../module/_config.php");
include("../../module/SiXhEaD.Template.php");
include("../../module/SiXhEaD.Pagination.php");
include("../../module/_module.php");
include("../../module/_module_control.php");

/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/

$tp_index		=	"_tp_export.html";

/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/

$strExcelFileName    =    "contactus.xls";
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/x-msexcel; Name=\"$strExcelFileName\"");
header("Content-Disposition: inline; Filename=\"$strExcelFileName\"");
header("Pragma: no-cache");

/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/

$searchq			=	$_REQUEST[searchq];


/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/

$tp = new Template($tp_index);

$objDb = mysql_connect($strCfgDbHost,$strCfgDbUser,$strCfgDbPass); 
mysql_select_db($strCfgDbName);
if (!$objDb) { die('Could not connect: ' . mysql_error()); }
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");

$WHERE = " ";
if($searchq!="")	 $WHERE = " AND ((`Subject` Like '%$searchq%') OR (`Message` Like '%$searchq%') ) ";

$strSql	=	"SELECT * FROM $strCfgDbTableContactUs  WHERE IsDelete='N' $WHERE ORDER BY `ContactId` DESC";

$objResult = mysql_query($strSql);
if (!$objResult) { echo "$strSql"; die("\n Invalid query: " . mysql_error()); }
$tp->block("DATA");
while(@extract(mysql_fetch_assoc($objResult))) {

	$AddDate			=	YYYYMMDDHHMMSS2DDMMYYYYHHMM($AddDate);
	$tp->apply();
}
mysql_free_result($objResult);
mysql_close($objDb);
$tp->Display();
exit;
?>