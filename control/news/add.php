<?
/*------------------------------------------------------------------*/
/*- Require --------------------------------------------------------*/

include("../../module/_config.php");
include("../../module/SiXhEaD.Template.php");
include("../../module/SiXhEaD.Pagination.php");
include("../../module/_module.php");
include("../../module/_module_control.php");

/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/

$tp_add			=	"_tp_add.html";
$tp_complete	=	"_tp_complete.html";
$tp_error			=	"_tp_error.html";

/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/


$DefineWidth	=	$strCfgNewsWidth;
$DefineHeight	=	$strCfgNewsHeight;


/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/
$Send				=	$_REQUEST['Send'];
$Id					=	$_REQUEST['Id'];
$ThTitle			=	htmlspecialchars($_REQUEST['ThTitle']);
$ThShortDetail		=	htmlspecialchars($_REQUEST['ThShortDetail']);
$ThDetail			=	$_REQUEST['ThDetail'];
$EnTitle			=	htmlspecialchars($_REQUEST['EnTitle']);
$EnShortDetail		=	htmlspecialchars($_REQUEST['EnShortDetail']);
$EnDetail			=	$_REQUEST['EnDetail'];
$File1NameBak		=	$_REQUEST['File1NameBak'];
$ActivityDate		=	$_REQUEST['ActivityDate'];
if($_REQUEST['Active']){$Active ="Y";}else{$Active ="N";}

/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/
	$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
	if (!$conn) {die('Not connected : ' . mysql_error());}
	// make foo the current db
	$db_selected = mysql_select_db($strCfgDbName, $conn);
	if (!$db_selected) { die ('Can\'t use $strCfgDbName : ' . mysql_error());}
	mysql_query("SET NAMES UTF8");
	mysql_query("SET character_set_results=UTF8");

if (!$Send) { 
	$ActiveChecked						=	"checked";
	$CateHotNewsSelected				=	" ";
	$CatePromotionsSelected	=	" ";
	$CateActivitiesSelected			=	" ";
	$AboutDay	=	7;
	$tp = new Template($tp_add);
	if($Id){
		$sql="SELECT * FROM $strCfgDbTableNews WHERE NewsId='$Id'";
		$result=mysql_query($sql);
		@extract(mysql_fetch_array($result,MYSQL_ASSOC));
		if($Active=="N") $ActiveChecked="";
		$ActivityDate= YYYYMMDDHHMMSS2DDMMYYYYHHMM($ActivityDate);
		

		mysql_free_result($result);
		$Photo1ViewAndDeleteLink="<br><img src=\"$strCfgWebPathNews/$File1\" border=\"0\" ID=\"IMG1\" width=\"150\"><br><a 
		href=\"javascript:confirmBox('$strCfgWebPathNews/$File1');\" class=\"LinkCancel\">[View]</a>&nbsp;<a href=\"javascript:confirmBox('del_file.php?Field=File1&Id=$Id');\" 
		class=\"LinkCancel\">[Delete]</a>";

		$Photo2ViewAndDeleteLink="<br><img src=\"$strCfgWebPathNews/$File2\" border=\"0\" ID=\"IMG2\" width=\"150\"><br><a 
		href=\"javascript:confirmBox('$strCfgWebPathNews/$File2');\" class=\"LinkCancel\">[View]</a>&nbsp;<a href=\"javascript:confirmBox('del_file.php?Field=File2&Id=$Id');\" 
		class=\"LinkCancel\">[Delete]</a>";

		$Photo3ViewAndDeleteLink="<br><img src=\"$strCfgWebPathNews/$File3\" border=\"0\" ID=\"IMG3\" width=\"150\"><br><a 
		href=\"javascript:confirmBox('$strCfgWebPathNews/$File3');\" class=\"LinkCancel\">[View]</a>&nbsp;<a href=\"javascript:confirmBox('del_file.php?Field=File3&Id=$Id');\" 
		class=\"LinkCancel\">[Delete]</a>";

		$Photo4ViewAndDeleteLink="<br><img src=\"$strCfgWebPathNews/$File4\" border=\"0\" ID=\"IMG4\" width=\"150\"><br><a 
		href=\"javascript:confirmBox('$strCfgWebPathNews/$File4');\" class=\"LinkCancel\">[View]</a>&nbsp;<a href=\"javascript:confirmBox('del_file.php?Field=File4&Id=$Id');\" 
		class=\"LinkCancel\">[Delete]</a>";

		$Photo5ViewAndDeleteLink="<br><img src=\"$strCfgWebPathNews/$File5\" border=\"0\" ID=\"IMG5\" width=\"150\"><br><a 
		href=\"javascript:confirmBox('$strCfgWebPathNews/$File5');\" class=\"LinkCancel\">[View]</a>&nbsp;<a href=\"javascript:confirmBox('del_file.php?Field=File5&Id=$Id');\" 
		class=\"LinkCancel\">[Delete]</a>";

		$Photo6ViewAndDeleteLink="<br><img src=\"$strCfgWebPathNews/$File6\" border=\"0\" ID=\"IMG6\" width=\"150\"><br><a 
		href=\"javascript:confirmBox('$strCfgWebPathNews/$File6');\" class=\"LinkCancel\">[View]</a>&nbsp;<a href=\"javascript:confirmBox('del_file.php?Field=File6&Id=$Id');\" 
		class=\"LinkCancel\">[Delete]</a>";
		
		mysql_close($conn);



	}
	if($ActivityDate==""){
		$ActivityDate=date("d/m/Y");
	}
	$tp->Display();
	
	exit;	

} else {
	
	$ActivityDate=DDMMYYYY2YYYYMMDD($ActivityDate);
	if($Id){
		$sql_update="";
		for($i=1;$i<=6;$i++){
		// - Upload  Image
	
			$strFileField			=	"File".$i;
			$NewFileName			=	"$Id-File".$i;
			$strUploadToPath		=	"$strCfgUploadPathNews";
			$strOnlyFileType		=	"image"; # File type header. Use | for more type eg. txt|pdf|doc 
			$intMaxFileSize			=	2097152; # 2M
			if ($_FILES["$strFileField"]["tmp_name"]) {
				list($owidth, $oheight)	= getimagesize($_FILES["$strFileField"]["tmp_name"]);
				
				/*if($owidth>$DefineWidth){
				
					$error	 ="<center>รูปภาพประกอบ : ความกว้างควรไม่เกิน ".$DefineWidth." pixel</center>";
					$tp = new Template($tp_error);
					$tp->Display();
					exit;	
				}
				if($oheight>$DefineHeight){
					
					$error	 ="<center>รูปภาพประกอบ : ความสูงควรไม่เกิน ".$DefineHeight." pixel</center>";
					$tp = new Template($tp_error);
					$tp->Display();
					exit;	
				}*/
				$strUploadFileArr	=	upload_file($strFileField,$NewFileName,$strUploadToPath,$strOnlyFileType,$intMaxFileSize);
				$NewFileName=$strUploadFileArr[name];
				if($owidth>$DefineWidth){
						resizeImageUpload($strCfgUploadPathNews,$NewFileName,$strCfgUploadPathNews,$NewFileName,$DefineWidth);
				}
				//$File_Name1 = $NewFileName;
				$sql_update	.=	",`File".$i."`='".$NewFileName."' ";
				$k++;
			}
		}
			

		$sql="UPDATE $strCfgDbTableNews SET `ThTitle`='$ThTitle',`ThShortDetail`='$ThShortDetail',`ThDetail`='$ThDetail',`EnTitle`='$EnTitle',`EnShortDetail`='$EnShortDetail',`EnDetail`='$EnDetail'".$sql_update.",`ActivityDate`='$ActivityDate',`Active`='$Active' 
		WHERE `NewsId`='$Id'";
		mysql_query($sql);
	//	echo $sql;
	}else{
	
	

		$sql="INSERT INTO 
		$strCfgDbTableNews(`IsDelete`,`ThTitle`,`ThShortDetail`,`ThDetail`,`EnTitle`,`EnShortDetail`,`EnDetail`,`File1`,`ActivityDate`,`Active`,`AddDate`)VALUES('N','$ThTitle','$ThShortDetail','$ThDetail','$EnTitle','$EnShortDetail','$EnDetail','$File_Name1','$ActivityDate','N',Now())";
		$result=mysql_query($sql);
//		echo "$sql";
		$Id=mysql_insert_id();
		$sql_update="";
		for($i=1;$i<=6;$i++){
	
		// - Upload  Image

			$strFileField			=	"File".$i;
			$NewFileName			=	"$Id-File".$i;
			$strUploadToPath		=	"$strCfgUploadPathNews";
			$strOnlyFileType		=	"image"; # File type header. Use | for more type eg. txt|pdf|doc 
			$intMaxFileSize			=	2097152; # 2M
			if ($_FILES["$strFileField"]["tmp_name"]) {

				list($owidth, $oheight)	= getimagesize($_FILES["$strFileField"]["tmp_name"]);
				/*if($owidth>$DefineWidth){
				
					$error	 ="<center>รูปภาพประกอบ : ความกว้างควรไม่เกิน ".$DefineWidth." pixel</center>";
					$tp = new Template($tp_error);
					$tp->Display();
					exit;	
				}
				
				if($oheight>$DefineHeight){
					
					$error	 ="<center>รูปภาพประกอบ : ความสูงควรไม่เกิน ".$DefineHeight." pixel</center>";
					$tp = new Template($tp_error);
					$tp->Display();
					exit;	
				}*/
				$strUploadFileArr	=	upload_file($strFileField,$NewFileName,$strUploadToPath,$strOnlyFileType,$intMaxFileSize);
				$NewFileName=$strUploadFileArr[name];
				if($owidth>$DefineWidth){
						resizeImageUpload($strCfgUploadPathNews,$NewFileName,$strCfgUploadPathNews,$NewFileName,$DefineWidth);
				}
				
				//$File_Name1 = $NewFileName;
				if($k>0){$sql_update	.=	",";}
				$sql_update	.=	"`File".$i."`='".$NewFileName."' ";
				$k++;
			}
		}

		$sql="UPDATE $strCfgDbTableNews SET ".$sql_update.",`Active`='$Active' WHERE `NewsId`='$Id'";
		mysql_query($sql);
		
	}
	
	mysql_close($conn);
	$tp = new Template($tp_complete);
	$tp->Display();
	
	exit;
}

/*------------------------------------------------------------------*/
?>