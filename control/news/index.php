<?
/*------------------------------------------------------------------*/
/*- Require --------------------------------------------------------*/

include("../../module/_config.php");
include("../../module/SiXhEaD.Template.php");
include("../../module/SiXhEaD.Pagination.php");
include("../../module/_module.php");
include("../../module/_module_control.php");


/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/

$tp_index		=	"_tp_index.html";

/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/


/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/

$Send			=	$_REQUEST['Send'];
$range			=	$_REQUEST['range'];
$searchq		=	$_REQUEST[searchq];
$order_by	=	$_REQUEST[order_by];
$order			=	$_REQUEST[order];
$LimitPage	=	$_REQUEST[LimitPage];
$page			=	$_REQUEST[page];
$cate				=	$_REQUEST[cate];

/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/
$tp = new Template($tp_index);
$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
if (!$conn) {die('Not connected : ' . mysql_error());}
// make foo the current db
$db_selected = mysql_select_db($strCfgDbName, $conn);
if (!$db_selected) { die ('Can\'t use $strCfgDbName : ' . mysql_error());}
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");
//- จัด Range ใหม่

if($Send=="Y"){

	if($range!=""){
			

		$arr_range=explode(",",$range);
		$k=1;
		for($i=0;$i<count($arr_range);$i++){
			if(intval($arr_range[$i])>0){
		
				$Id=intval($arr_range[$i]);
				$sql="UPDATE $strCfgDbTableNews SET LineNo='$k' WHERE NewsId='$Id'";
				mysql_query($sql);
				$k++;
			}

		}

	}
}

/*-----------------------------------------------------------------*/


if(!$LimitPage){$LimitPage=30;}
//- Condition
$WHERE = " ";

if($searchq){$WHERE	.=	"AND ( ThTitle  LIKE '%$searchq%'  OR ThDetail LIKE '%$searchq%') OR ( EnTitle  LIKE '%$searchq%'  OR EnDetail LIKE '%$searchq%') ";}

//- Order By
$order_by = " Mark DESC,LineNo ASC,`NewsId` DESC ";


$sp	= New Pagination();
$sp->db_type			=	"MySQL";
$sp->db_table			=	$strCfgDbTableNews;
$sp->order_by			=	$order_by;
$sp->primary_key	=	"NewsId";
$sp->select			=	"`NewsId`,ActivityDate,`LineNo`,`EnTitle`,`ThTitle`,`Active`,`Mark`,`AddDate`,`UpdateDate`";
$sp->custom_sql		=	" WHERE IsDelete='N'  $WHERE " ; # WHERE, JOIN
$sp->custom_param	=	"&LimitPage=$LimitPage&cate=$cate&searchq=$searchq";
$sp->per_page		=	$LimitPage;
$sp->order_param	=	"order"; # $_GET["order"];
$sp->page_param	=	"page"; # $_REQUEST["p"];
$sp->page_prev		=	"&#8249; Prev";
$sp->page_next		=	"Next &#8250;";
$sp->page_first		=	"&laquo; หน้าแรก";
$sp->page_last		=	"หน้าสุดท้าย &raquo;";
$sp->sign_left			=	"";
$sp->sign_right		=	" |";
$sp->sign_trim		=	FALSE;
$sp->style				=	1;
$sp->start();

$intRecordAll		=	$sp->get_record_all();
$sql				=	$sp->get_sql();
$strPageLink		=	$sp->get_page_link();

$bg_color = "";
$result = mysql_query($sql); //echo $sql;
if (!$result) { echo "$sql"; die('Invalid query: ' . mysql_error()); }
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");

$tp->block("DATA");
$i=0;
while($row = mysql_fetch_array($result)) {
	$NewsId				=	$row[NewsId];
	$TblID = <<<Data
ID="$NewsId";
Data;
	$LineNo				=	$row['LineNo'];
	$ThTitle			=	$row['ThTitle'];
	//$EnTitle			=	$row['EnTitle'];
	$Active				=	$row['Active'];
	$Mark				=	$row['Mark'];
	$ActivityDate		=	YYYYMMDDHHMMSS2DDMMYYYYHHMM($row['ActivityDate']);
	$AddDate			=	YYYYMMDDHHMMSS2DDMMYYYYHHMM($row['AddDate']);
	$UpdateDate		=	YYYYMMDDHHMMSS2DDMMYYYYHHMM($row['UpdateDate']);
	
	// - Active 
	if ($Active == "Y") {
		$strActiveNew	=	"N";
		$strActive		=	"$Active";
		$strActiveLinkCss=	"LinkYes";
	}
	else {
		$strActiveNew	=	"Y";
		$strActive		=	"$Active";
		$strActiveLinkCss=	"LinkNo";
	}
	$strBgColor = "#FFFFFF";
	$strBgColorOver = "#D9FFD9";

	// - Mark
	if ($Mark == "Y") {
		$strMarkNew	=	"N";
		$strMark	=	"$Mark";
		$strMarkLinkCss=	"LinkYes";

		$strBgColor = "#FFFFCC";
		$strBgColorOver = "#FF3333";
	}
	else {
		$strMarkNew	=	"Y";
		$strMark	=	"$Mark";
		$strMarkLinkCss=	"LinkNo";
	}

	//-Color Row
	
	$strCssTr = " bgcolor=\"$strBgColor\" onmouseover=\"this.style.background='$strBgColorOver'\" onmouseout=\"this.style.background='$strBgColor'\" ";
	$tp->apply();
	$i++;
}
mysql_free_result($result);
mysql_close($conn);



$tp->Display();
exit;

/*------------------------------------------------------------------*/
?>