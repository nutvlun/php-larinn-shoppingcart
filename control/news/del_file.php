<?
/*------------------------------------------------------------------*/
/*- Require --------------------------------------------------------*/

include("../../module/_config.php");
include("../../module/SiXhEaD.Template.php");
include("../../module/SiXhEaD.Pagination.php");
include("../../module/_module_control.php");
/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/

$tp_del_file		=	"_tp_del_file.html";
$tp_complete		=	"_tp_complete.html";

/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/

authenAdmin();

/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/

$Confirm	=	$_REQUEST['Confirm'];
$Id			=	$_REQUEST['Id'];
$Field		=	$_REQUEST['Field'];

/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/

$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
	if (!$conn) {die('Not connected : ' . mysql_error());}
	// make foo the current db
	$db_selected = mysql_select_db($strCfgDbName, $conn);
	if (!$db_selected) { die ('Can\'t use $strCfgDbName : ' . mysql_error());}
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");

if($Confirm!=1){
	$tp = new Template($tp_del_file);
	$sql="SELECT $Field FROM $strCfgDbTableNews WHERE NewsId='$Id'";
	$result=mysql_query($sql);
	while($row=mysql_fetch_array($result,MYSQL_NUM)){
		$ImageNeedDelete="<img src=\"$strCfgWebPathNews/".$row[0]."\" border=\"0\" width=\"300\">";
	}
	mysql_free_result($result);
	mysql_close($conn);
	$tp->Display();
	exit;
}
if($Confirm==1){
	$sql="SELECT $Field FROM $strCfgDbTableNews WHERE NewsId='$Id'";
	$result=mysql_query($sql);
	while($row=mysql_fetch_array($result,MYSQL_NUM)){
		$ImageNeedDelete=$row[0];
		if ($ImageNeedDelete) {
			if (file_exists($strCfgUploadPathNews."/$ImageNeedDelete")) { 
				unlink ($strCfgUploadPathNews."/$ImageNeedDelete");
			}
		}
	}
	mysql_free_result($result);
	$sql="UPDATE $strCfgDbTableNews SET $Field ='' WHERE NewsId='$Id'";
	mysql_query($sql);
	mysql_close($conn);
	$tp = new Template($tp_complete);
	$tp->Display();
	exit;
}

/*------------------------------------------------------------------*/
?>