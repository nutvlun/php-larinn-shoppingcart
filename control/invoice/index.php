<?
/*------------------------------------------------------------------*/
/*- Require --------------------------------------------------------*/

include("../../module/_config.php");
include("../../module/SiXhEaD.Template.php");
include("../../module/SiXhEaD.Pagination.php");
include("../../module/_module.php");
include("../../module/_module_control.php");


/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/

$tp_index		=	"_tp_index.html";

/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/


/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/

$Send			=	$_REQUEST['Send'];
$range			=	$_REQUEST['range'];
$searchq		=	$_REQUEST[searchq];
$order_by		=	$_REQUEST[order_by];
$order			=	$_REQUEST[order];
$LimitPage		=	$_REQUEST[LimitPage];
$InvStatus		=	$_REQUEST[InvStatus];
$PayBy			=	$_REQUEST[PayBy];
$page			=	$_REQUEST[page];

/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/
$tp = new Template($tp_index);
$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
if (!$conn) {die('Not connected : ' . mysql_error());}
// make foo the current db
$db_selected = mysql_select_db($strCfgDbName, $conn);
if (!$db_selected) { die ('Can\'t use $strCfgDbName : ' . mysql_error());}
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");
//- จัด Range ใหม่

if($Send=="Y"){

	
}

/*-----------------------------------------------------------------*/


if(!$LimitPage){$LimitPage=50;}
//- Condition
$WHERE = " ";
if($searchq){$WHERE	.=	"AND ( $strCfgDbTableMember.`Email`  LIKE '%$searchq%'  OR $strCfgDbTableMember.`Fname` LIKE '%$searchq%') ";}

if($InvStatus){$WHERE	.=	"AND ( $strCfgDbTableInvH.`Status`  = '$InvStatus') ";}

if($PayBy){$WHERE	.=	"AND ( $strCfgDbTableInvH.`PayBy`  = '$PayBy') ";}

//- Order By
$order_by = "$strCfgDbTableInvH.`InvHId` DESC ";

$sp	= New Pagination();
$sp->db_type			=	"MySQL";
$sp->db_table			=	"$strCfgDbTableInvH";
$sp->order_by			=	$order_by;
$sp->primary_key	=	"`InvHId`";
$sp->select			=	"$strCfgDbTableInvH.`InvHId` As `InvHId`,$strCfgDbTableInvH.`InvNo` As `InvNo`,$strCfgDbTableInvH.`MemberId` As `MemberId`,$strCfgDbTableInvH.`Total` As `Total`,$strCfgDbTableInvH.`PayBy` As `PayBy`,$strCfgDbTableInvH.`Status` As `Status`,$strCfgDbTableMember.`Fname` As `Fname`,$strCfgDbTableMember.`Lname` As `Lname`,$strCfgDbTableMember.`Email` As `Email`,$strCfgDbTableInvH.`AddDate` As `AddDate`";
$sp->custom_sql		=	" INNER JOIN $strCfgDbTableMember ON $strCfgDbTableInvH.`MemberId`= $strCfgDbTableMember.`MemberId` WHERE $strCfgDbTableInvH.IsDelete='N' $WHERE " ; # WHERE, JOIN
$sp->custom_param	=	"&InvStatus=$InvStatus&PayBy=$PayBy&LimitPage=$LimitPage&searchq=$searchq";
$sp->per_page		=	$LimitPage;
$sp->order_param	=	"order"; # $_GET["order"];
$sp->page_param	=	"page"; # $_REQUEST["p"];
$sp->page_prev		=	"&#8249; Prev";
$sp->page_next		=	"Next &#8250;";
$sp->page_first		=	"&laquo; หน้าแรก";
$sp->page_last		=	"หน้าสุดท้าย &raquo;";
$sp->sign_left			=	"";
$sp->sign_right		=	" |";
$sp->sign_trim		=	FALSE;
$sp->style				=	1;
$sp->start();

$intRecordAll		=	$sp->get_record_all();
$sql				=	$sp->get_sql();
$strPageLink		=	$sp->get_page_link();

$bg_color = "";
$result = mysql_query($sql); //echo $sql;
if (!$result) { echo "$sql"; die('Invalid query: ' . mysql_error()); }
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");

$tp->block("DATA");
$i=0;
while($row = mysql_fetch_array($result)) {
	$Id				=	$row[InvHId];
	$TblID = <<<Data
ID="$Id";
Data;
	$InvNo				=	$row["InvNo"];
	$PayBy				=	($row["PayBy"]=="bank")?"Bank":"PayPal";
	switch ($row["Status"]) {
    case "wait_pay":
       $Status		= "รอการชำระเงิน";
        break;
    case "pay_completed":
         $Status	="ชำระเงินเรียบร้อยแล้ว";
        break;
    case "wait_ship":
        $Status		= "กำลังจัดส่ง";
        break;
	 case "shiped":
         $Status	= "จัดส่งแล้ว";
        break;
	case "cancel":
         $Status	= "ยกเลิกรายการ";
        break;
	}
	$Total				=	number_format($row["Total"],2, '.', ',');
	$Fname				=	$row[Fname];
	$Lname				=	$row[Lname];
	$Email				=	$row[Email];

	$AddDate			=	YYYYMMDDHHMMSS2DDMMYYYYHHMM($row[AddDate]);
	
	
	// - Active 
	if ($Active == "Y") {
		$strActiveNew	=	"N";
		$strActive		=	"$Active";
		$strActiveLinkCss=	"LinkYes";
	}
	else {
		$strActiveNew	=	"Y";
		$strActive		=	"$Active";
		$strActiveLinkCss=	"LinkNo";
	}

	//-Color Row
	$strBgColor = "#FFFFFF";
	$strBgColorOver = "#D9FFD9";
	$strCssTr = " bgcolor=\"$strBgColor\" onmouseover=\"this.style.background='$strBgColorOver'\" onmouseout=\"this.style.background='$strBgColor'\" ";
	$tp->apply();
	$i++;
}
mysql_free_result($result);
mysql_close($conn);



$tp->Display();
exit;

/*------------------------------------------------------------------*/
?>