<?
/*------------------------------------------------------------------*/
/*- Require --------------------------------------------------------*/

include("../../module/_config.php");
include("../../module/SiXhEaD.Template.php");
include("../../module/SiXhEaD.Pagination.php");
include("../../module/_module.php");
include("../../module/_module_control.php");

/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/

$tp_add			=	"_tp_add.html";
$tp_complete	=	"_tp_complete.html";

/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/




/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/
$Send			=	$_REQUEST['Send'];
$Id				=	$_REQUEST['Id'];
$Status				=	$_REQUEST['Status'];



/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/
$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
if (!$conn) {die('Not connected : ' . mysql_error());}
$db_selected = mysql_select_db($strCfgDbName, $conn);
if (!$db_selected) { die ('Can\'t use ' .$strCfgDbName.' : ' . mysql_error());}
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");

$ActiveChecked="checked";
if (!$Send) { 
	$tp = new Template($tp_add);
	$sql="SELECT $strCfgDbTableInvH.`InvNo` As `InvNo`,$strCfgDbTableInvH.`Status` As `Status`,$strCfgDbTableInvH.`Total` As `Total`,$strCfgDbTableInvH.`AddDate` As `AddDate`,$strCfgDbTableMember.`Email` As `Email`,$strCfgDbTableMember.`Fname` As `Fname`,$strCfgDbTableMember.`Lname` As `Lname`,$strCfgDbTableMember.`Address` As `Address`,$strCfgDbTableMember.`Province` As `Province`,$strCfgDbTableMember.`Zipcode` As `Zipcode`,$strCfgDbTableMember.`Phone` As `Phone` FROM $strCfgDbTableInvH INNER JOIN $strCfgDbTableMember ON $strCfgDbTableMember.`MemberId`=$strCfgDbTableInvH.`MemberId` WHERE $strCfgDbTableInvH.`InvHId`='$Id' AND $strCfgDbTableInvH.`IsDelete`='N' ";
$result	=	mysql_query($sql);
@extract(mysql_fetch_assoc($result));
$AddDate			=	YYYYMMDDHHMMSS2DDMMYYYYHHMM($AddDate);
$Total				=	number_format($Total,2, '.', ',');
$chk_wait_pay	=	"";
$chk_pay_completed	=	"";
$chk_wait_ship	=	"";
$chk_shiped	=	"";
$chk_cancel	=	"";

switch ($Status) {
    case "wait_pay":
       $chk_wait_pay		= "selected";
        break;
    case "pay_completed":
         $chk_pay_completed	="selected";
        break;
    case "wait_ship":
        $chk_wait_ship		= "selected";
        break;
	 case "shiped":
         $chk_shiped	= "selected";
        break;
	case "cancel":
         $chk_cancel	= "selected";
        break;
	}

$sql="SELECT $strCfgDbTableInvDtl.`InvDtlId` As `InvDtlId`,$strCfgDbTableInvDtl.`ProductId` As `ProductId`,$strCfgDbTableInvDtl.`ProductCode` As `ProductCode`,$strCfgDbTableInvDtl.`ProductName` As `ProductName`,$strCfgDbTableInvDtl.`Size` As `Size`,$strCfgDbTableInvDtl.`ColorId` As `ColorId`,$strCfgDbTableInvDtl.`Qty` As `Qty`,$strCfgDbTableInvDtl.`UnitPrice` As `UnitPrice`,$strCfgDbTableInvDtl.`SubTotal` As `SubTotal`,$strCfgDbTableProduct.`File1` As `File1`  FROM $strCfgDbTableInvDtl  Inner JOIN $strCfgDbTableProduct ON $strCfgDbTableProduct.`ProductId`=$strCfgDbTableInvDtl.`ProductId` WHERE $strCfgDbTableInvDtl.`InvHId`='$Id' ORDER BY $strCfgDbTableInvDtl.`InvDtlId` ASC";
$result	=	mysql_query($sql);
$tp->block("data");
while($row=mysql_fetch_assoc($result)){
	$InvDtlId	=	$row["InvDtlId"];
	$ProductId	=	$row["ProductId"];
	$ProductCode	=	$row["ProductCode"];
	$ProductName	=	$row["ProductName"];
	$Size	=	$row["Size"];
	$ColorId	=	$row["ColorId"];
	$Qty	=	$row["Qty"];
	$UnitPrice	=	$row["UnitPrice"];
	$SubTotal	=	$row["SubTotal"];
	$File1	=	$row["File1"];
	$Img=	"../../module/phpThumb/phpThumb.php?src=../../content/product/$File1&w=$strCfgProductThumbWidth&h=$strCfgProductThumbHeight&zc=1&sx=0";
	$tp->apply();
}
	$tp->Display();
	mysql_close($conn);	
	exit;
}	
if($Id){
	$sql="UPDATE $strCfgDbTableInvH SET Status='$Status' WHERE  `InvHId`= '$Id' ";
	$result=mysql_query($sql);
}


$tp = new Template($tp_complete);
$tp->Display();
mysql_close($conn);	
exit;

/*------------------------------------------------------------------*/
?>