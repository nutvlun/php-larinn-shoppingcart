<? //error_reporting(0);

# MySql config
if (preg_match("/localhost$/",$_SERVER[SERVER_NAME])) { 
	$strCfgDbHost		=	"localhost";
	$strCfgDbName		=	"larinn_db";
	$strCfgDbUser		=	"root";
	$strCfgDbPass		=	"";
	$strCfgFixNotRootPath	=	"/larinn";
	$strCfgPrefix	=	"";
}
elseif (preg_match("/larinn.bewebmoral.com$/",$_SERVER[SERVER_NAME])) { 
	$strCfgDbHost	=	"localhost";
	$strCfgDbName	=	"bewebmor_larinn";
	$strCfgDbUser	=	"bewebmor_larinn";
	$strCfgDbPass	=	"larinn@db";
	$strCfgPrefix	=	"";
	//$strCfgFixNotRootPath	=	"/demo";
}
else { 
	$strCfgDbHost	=	"localhost";
	$strCfgDbName	=	"larinn_db";
	$strCfgDbUser	=	"larinn_user";
	$strCfgDbPass	=	"3tIrC44q";
	$strCfgPrefix	=	"";
	//$strCfgFixNotRootPath	=	"/demo";
}
$strCfgDbTableAdmin				=	$strCfgPrefix."admin";
$strCfgDbTableBank				=	$strCfgPrefix."bank";
$strCfgDbTableCategory			=	$strCfgPrefix."category";
$strCfgDbTableColor				=	$strCfgPrefix."color";
$strCfgDbTableContactUs			=	$strCfgPrefix."contactus";
$strCfgDbTableInvH				=	$strCfgPrefix."inv_h";
$strCfgDbTableInvDtl			=	$strCfgPrefix."inv_dtl";
$strCfgDbTableKeyvisual			=	$strCfgPrefix."keyvisual";
$strCfgDbTableMember			=	$strCfgPrefix."member";
$strCfgDbTableNews				=	$strCfgPrefix."news";
$strCfgDbTableProduct			=	$strCfgPrefix."product";
$strCfgDbTableSubCategory		=	$strCfgPrefix."sub_category";
$strCfgDbTableSize				=	$strCfgPrefix."size";



#WebSite Config

//$strCfgFixPort						=	":80";
$strCfgFixPort							=	"";
$strCfgMainUrl							=	"http://" . $_SERVER[SERVER_NAME] . "$strCfgFixPort" . $strCfgFixNotRootPath;
$strCfgDocumentRoot						=	$_SERVER[DOCUMENT_ROOT] . $strCfgFixNotRootPath;
$strCfgServerIp							=	$_SERVER[SERVER_ADDR];
$strCfgMainUrlControl					=	"$strCfgMainUrl/control";
$strCfgWebPathNews						=	"$strCfgMainUrl/content/news";
$strCfgUploadPathNews					=	"$strCfgDocumentRoot/content/news";
$strCfgWebPathProduct					=	"$strCfgMainUrl/content/product";
$strCfgUploadPathProduct				=	"$strCfgDocumentRoot/content/product";
$strCfgWebPathKeyvisual					=	"$strCfgMainUrl/content/keyvisual";
$strCfgUploadPathKeyvisual				=	"$strCfgDocumentRoot/content/keyvisual";


$strCfgKeyVisualWidth	 =	760;
$strCfgKeyVisualHeight	 =	315;

$strCfgNewsThumbWidth				=	270;
$strCfgNewsThumbHeight				=	160;
$strCfgNewsHeight					=	600;
$strCfgNewsWidth					=	363;

$strCfgProductThumbWidth			=	183;
$strCfgProductThumbHeight			=	197;
$strCfgProductSmallThumbWidth		=	67;
$strCfgProductSmallThumbHeight		=	72;
$strCfgProductWidth					=	396;
$strCfgProductHeight				=	426;


# /WebSite Config



# SMTP Config #


$strCfgSmtp				=	true;
$strCfgSmtpHost			=	"localhost";
$strCfgSmtpSMTPAuth		=	true;
$strCfgSmtpUsername		=	"no-reply@larinn.com"; #
$strCfgSmtpPassword		=	"2D90adnn"; #

# /SMTP Config #

# Email Config #
$strCfgEmailFromName		=	"Larinn.com";
$strCfgEmailFromEmail		=	"no-reply@larinn.com";
$strCfgEmailContactUs		=	"contact@larinn.com";

/* Cache */
$strCfgCacheActive			=	true;
$strCfgCacheFolder			=	"$strCfgDocumentRoot/_cache";
$strCfgWebPathCache			=	"$strCfgMainUrl/_cache";
/* / Cache */

# Define String #
$OwnerUrl							=	"Copyright © 2013 larinn.com All rights reserved.";
$strCfgTextUserLoginError			=	"Invalid Username Or Password";
$strCfgTextUserSessionExpire		=	"Session Expire";


?>