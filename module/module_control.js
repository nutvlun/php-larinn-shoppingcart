<!--//
var timerID = false;

function hide_sub () {
	$('#sub_menu_config').hide();

}

function setDestroyNavMenuTimer () {
	timerID = setTimeout("hide_sub();",700);
}

/*### Popup ####*/
function PopUpWindow(URL, N, W, H, S) { // name, width, height, scrollbars
	var winleft	=	(screen.width - W) / 2;
	var winup	=	(screen.height - H) / 2;
	winProp		=	'width='+W+',height='+H+',left='+winleft+',top='+winup+',scrollbars='+S+',resizable' + ',status=yes'
	Win			=	window.open(URL, N, winProp)
	if (parseInt(navigator.appVersion) >= 4) { Win.window.focus(); }
}

function controlContentAdd(strUrl) {
	PopUpWindow(strUrl,'Add',800,650,'yes');
}
function controlContentAddMax(strUrl) {
	PopUpWindow(strUrl,'AddMax',1020,650,'yes');
}

function controlContentAddMini(strUrl) {
	PopUpWindow(strUrl,'AddMini',550,350,'yes');
}

function controlContentEdit(strUrl) {
	PopUpWindow(strUrl,'Edit',800,650,'yes');
}
function controlContentEditMini(strUrl) {
	PopUpWindow(strUrl,'EditMini',550,350,'yes');
}

function confirmContentList(strUrl) {
	PopUpWindow(strUrl,'ContentList',800,650,'yes');
}


function confirmBox(strUrl) {
	PopUpWindow(strUrl,'ConfirmBox',500,150,'yes');
}

function controlExport(strUrl) {
	PopUpWindow(strUrl,'Export',500,350,'yes');
}

function controlImport(strUrl) {
	PopUpWindow(strUrl,'Import(',500,350,'yes');
}

function confirmSend(strUrl) {
	PopUpWindow(strUrl,'Send',500,350,'yes');
}


function controlContentReject(strUrl) {
	PopUpWindow(strUrl,'Reject',950,350,'yes');
}

function controlContentPreview(strUrl) {
	PopUpWindow(strUrl,'Reject',950,500,'yes');
}

function jumpMenu(targ,selObj,restore){   
	if (selObj.options[selObj.selectedIndex].value != "") {
		eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
		if (restore) selObj.selectedIndex=0;
	}
}

$(document).ready(function() {
	$('#menu_config').mouseover(function(){
		var nav_id = "menu_config";
		var navMenuData	=	'#sub_'+ nav_id ;	
		var navMenuPosition =$('#'+nav_id).position();
		var navMenuHeight = $('#'+nav_id).height();
		var navMenuWidth = $('#'+nav_id).width();
		var navMenuDataTop	=	(navMenuPosition.top + navMenuHeight)+15;
		var navMenuDataLeft	=	navMenuPosition.left ;
		clearTimeout(timerID);
		$("#sub_menu_config").css({ top: navMenuDataTop +"px", left: navMenuDataLeft +"px", position:"absolute" });	
		$('#sub_menu_config').show();
	}).mouseout(function(){
		 setDestroyNavMenuTimer ();
  });
	$("#sub_menu_config").mouseover(function() {
	   clearTimeout(timerID);
    
	}).mouseout(function() {
		setDestroyNavMenuTimer();		
	});
});

//-->