<?
session_start();
# -- Global Var -- #
# -- /Global Var -- #

authenAdmin ();
# -- Global Var -- #
$sAdminMenu			=	getAdminMenu();

# -- /Global Var -- #

# -- Library -- #
function getAdminMenu() {	
	global $strCfgLibraryAdminMenu;
	global $sAdminType;
	if($_SESSION[strCfgLibraryAdminMenu]){$strCfgLibraryAdminMenu =  $_SESSION[strCfgLibraryAdminMenu];}
	$tp = new Template($strCfgLibraryAdminMenu);
	return $tp->Generate();
}

function authenAdmin () {
	global $strCfgMainUrlControl;
	global $sAdminUsername;
	global $sAdminType;
	global $sAdminMenu;
	$sAdminUsername	=	$_SESSION['sAdminUsername'];
	$AdminType			=	$_SESSION['sAdminType'];

	$Go = urlencode($_SERVER["REQUEST_URI"]);
	if (!isset($_SESSION['sAdminId'])) { 
		header("Location: $strCfgMainUrlControl/login.php?Go=$Go");
		exit;
	}
}
?>