<?
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*

Program     : iSC (intelligence Secret Code)
Description : 
Programmer  : ���ѵ��  �ع�����ҵ

Individual
Email       : pipo@sixhead.com
Website     : http://www.sixhead.com
            : http://www.todaysoftware.com

Office
Email       : pipo@digithais.com
Website     : http://www.digithais.com

Date        : 11/10/2006 v1.0 First Release
Modify log  : 12/10/2006 v1.1 Customize font, font color, bg, angle

*/
/*------------------------------------------------------------------*/
/*- Class ----------------------------------------------------------*/

class iSC {

	function Create($intRandomKeyLength = 4) {
		$strRandomKey		=	"";
		#$strInputArr		=	array ("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",0,1,2,3,4,5,6,7,8,9);
		$strInputArr		=	array ("A","B","C","D","E","F","G","H","J","K","L","M","N","P","Q","R","S","T","U","V","W","X","Y","Z",2,3,4,5,6,7,8,9);
		$strRandomKeyArr	=	array_rand ($strInputArr, $intRandomKeyLength);
		for ($intI=0;$intI<=$intRandomKeyLength-1;$intI++) {
			$strRandomKey	.=	$strInputArr[$strRandomKeyArr[$intI]];
		}
		$this->strRandomKey			=	$strRandomKey;
	}


	function GetKey() {
		return $this->strRandomKey;
	}


	function SetFont($strFontFile="tahoma.ttf",$intFontSize=30,$intLeftMargin=10,$intTopMargin=40) {
		$this->strFontFile	=	"./font/" . $strFontFile;
		$this->intFontSize	=	$intFontSize;
		$this->intLeftMargin=	$intLeftMargin;
		$this->intTopMargin	=	$intTopMargin;
	}


	function SetFontColor($strFontColorArr = array ("#006600","#006699","#CC0000","#669933","#666666")) {
		$this->strFontColorArr = $strFontColorArr;
	}


	function SetAllBg($strAllBgArr = array ("01","02","03")) {
		$this->strAllBgArr = $strAllBgArr;
	}


	function SetAllAngle($strAllAngleArr = array (-5,-4,-3,-2,-1,1,2,3,4,5)) {
		$this->strAllAngleArr = $strAllAngleArr;
	}


	function Display() {		
		if ($this->strFontFile == "") { 
			$this->SetFont();
		}

		$strRandomBg			=	$this->RandomBg();
		$strRandomAngle			=	$this->RandomAngle();
		$strRandomFontColorArr	=	$this->RandomFontColor();

		$Image					=	imagecreatefromjpeg ("./bg/$strRandomBg.jpg");
		$FontColor				=	imagecolorallocate($Image, $strRandomFontColorArr[0], $strRandomFontColorArr[1], $strRandomFontColorArr[2]);

		imagestring ( $Image, 14, 10,5, $this->strRandomKey, $FontColor );
		//imagettftext ($Image, $this->intFontSize, $strRandomAngle, $this->intLeftMargin, $this->intTopMargin, $FontColor, $this->strFontFile, $this->strRandomKey);

		header("Cache-Control: no-cache, must-revalidate");
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");		
		header("Content-type: image/jpeg");
		imagejpeg($Image,"",100);
		imagedestroy($Image);
	}


	function RandomAngle() {		
		if (!is_array($this->strAllAngleArr)) { $this->SetAllAngle(); }	

		$intRandom		=	(rand() % sizeof($this->strAllAngleArr));
		$intAngle		=	$this->strAllAngleArr[$intRandom];
		return $intAngle;
	}


	function RandomBg() {
		if (!is_array($this->strAllBgArr)) { $this->SetAllBg(); }	

		$intRandom		=	(rand() % sizeof($this->strAllBgArr));
		$strBg			=	$this->strAllBgArr[$intRandom];
		return $strBg;
	}


	function RandomFontColor() {		
		if (!is_array($this->strFontColorArr)) { $this->SetFontColor(); }	

		$intRandom				=	array_rand ($this->strFontColorArr, 1);
		$strRandomFontColor		=	$this->strFontColorArr[$intRandom];

		$strRandomFontColor		=	preg_replace("/#/", "", $strRandomFontColor);
		$strRandomFontColor		=	preg_replace("/(\w\w)(\w\w)(\w\w)/", "\\1,\\2,\\3", $strRandomFontColor);
		$strRandomFontColorArr	=	split (",",$strRandomFontColor);

		$strRandomFontColorArr[0]	=	base_convert($strRandomFontColorArr[0],16,10);
		$strRandomFontColorArr[1]	=	base_convert($strRandomFontColorArr[1],16,10);
		$strRandomFontColorArr[2]	=	base_convert($strRandomFontColorArr[2],16,10);
		

		return $strRandomFontColorArr;
	}

}


/*------------------------------------------------------------------*/
?>