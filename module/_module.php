<?php

session_start();
sixhead();
if(!isset($_SESSION["s_inCart"]))$_SESSION["s_inCart"]=0;
$inCart	=	intval($_SESSION["s_inCart"]);

function checkID($id) {
    if(strlen($id) != 13) return false;
    for($i=0, $sum=0; $i<12;$i++)
        $sum += (int)($id{$i})*(13-$i);
    if((11-($sum%11))%10 == (int)($id{12}))
        return true;
    return false;
}
function CheckLogin(){
	global $strCfgDocumentRoot;
	$strCfgLibraryMemberMenu	=	$strCfgDocumentRoot."/include/member-menu.html";
	$tp = new Template($strCfgLibraryMemberMenu);
	if($_SESSION["su_member_id"]){
		$tp->block("Login");
		$tp->apply();
	}else{
		$tp->block("NoLogIn");
		$tp->apply();
	}
	return set_full_url ($tp->Generate());
}

function set_full_url ($str) {
	global $strCfgMainUrl;

	$str	=	preg_replace("/\.\.\/\.\.\/\.\.\//", $strCfgMainUrl . "/",$str);
	$str	=	preg_replace("/\.\.\/\.\.\//", $strCfgMainUrl . "/",$str);
	$str	=	preg_replace("/\.\.\//", $strCfgMainUrl . "/",$str);

	$str	=	preg_replace("/<a href='\//", "<a href='$strCfgMainUrl/",$str);
	$str	=	preg_replace("/<a href=\"\//", "<a href=\"$strCfgMainUrl/",$str);

	$str	=	preg_replace("/\'\/images\//", "'$strCfgMainUrl/images/",$str);
	$str	=	preg_replace("/\"\/images\//", "\"$strCfgMainUrl/images/",$str);
	$str	=	preg_replace("/\'images\//", "'$strCfgMainUrl/images/",$str);
	$str	=	preg_replace("/\"images\//", "\"$strCfgMainUrl/images/",$str);

	return $str;
}


function authen_user () {
	global $strCfgMainUrl;

	$go = urlencode($_SERVER["REQUEST_URI"]);
	if (!isset($_SESSION["su_member_id"])) { 
		header("Location: $strCfgMainUrl/member/login.php?go=$go");
		exit;
	}
}
function authen_user_popup () {
	global $strCfgMainUrl;

	$go = urlencode($_SERVER["REQUEST_URI"]);
	if (!isset($_SESSION["su_memberid"])) { 
		//header("Location: $strCfgMainUrl/member/login.php?go=$go&popup=yes");
		echo "<script type=\"text/javascript\">
		<!--
			parent.location.href ='$strCfgMainUrl/member/login.php?go=$go';
		//-->
		</script>";
		exit;
	}
}

function debug($str) {
	echo "<pre>";
	print_r($str);
	echo "</pre>";
}

function comify($number,$is_float=FALSE) {
	if ($is_float == true)
	{
	return number_format($number,2,'.',',');
	}
	else 
	{
	return number_format($number,0,'.',',');
	}
}



function sixhead () {
	header("X-Program-By: pipo@sixhead.com");
	header("X-Url: http://sixhead.com");
}

function upload_file($file_field,$file_new_name="",$upload_to_path="",$only_file_type="",$max_file_size=2097152)
{

	if (!$_FILES["$file_field"]["name"]) {
		return;
	}

	$file_name		=	$_FILES["$file_field"]["name"];
	$file_type		=	$_FILES["$file_field"]["type"];
	$file_size		=	$_FILES["$file_field"]["size"];
	$file_tmp_name	=	$_FILES["$file_field"]["tmp_name"];
	$file_error		=	$_FILES["$file_field"]["error"];
	$error_template	=	"Upload file error reason:";
	
	if ($file_error) { 
		echo "$error_template $file_error";
		exit;
	}		
	if ($only_file_type) { 
		if (!(ereg($only_file_type,$file_type))) { 
			echo "$error_template this file type '$file_type' Only allow $only_file_type";
			exit;
		}
	}

	$file_names = split ("\.", basename($file_name));
	list($upload_file_extension,$upload_file_name) = array_reverse($file_names);
	$upload_file_extension = strtolower($upload_file_extension);
	

	if ($file_new_name == "") { // ไม่ได้ตั้งชื่อไฟล์ใหม่ให้ใช้ชื่อไฟล์เดิม
		$file_new_name	=	$upload_file_name;
	}
	if ($upload_to_path == "") { 
			$upload_to_path	= dirname($_SERVER["PATH_TRANSLATED"]);
	}
	if ($file_size > $max_file_size) { 
		echo "$error_template Max file size = $max_file_size bytes";
		exit;
	}
	if (ereg("image",$file_type) || ereg("flash",$file_type)) { 
		list($width, $height) = getimagesize($file_tmp_name);
	}

	$file_new_name = "$file_new_name.$upload_file_extension";
	$file_upload_to	= "$upload_to_path/$file_new_name";
	move_uploaded_file($file_tmp_name,$file_upload_to); 
	
	$upload_file_info["name"]	=	$file_new_name;
	$upload_file_info["type"]	=	$file_type;
	$upload_file_info["size"]	=	$file_size;
	$upload_file_info["width"]	=	$width;
	$upload_file_info["height"]	=	$height;

	return $upload_file_info;
}
function cropImage($nw, $nh, $source, $dest) {//Crop รูปที่ Upload แล้ว
	$size = getimagesize($source);
	$w = $size[0];
	$h = $size[1];
	switch($size[2]) {
	case 1:
		$simg = imagecreatefromgif($source);
		break;
	case 2:
		$simg = imagecreatefromjpeg($source);
		break;
	case 3:
		$simg = imagecreatefrompng($source);
		break;
	}
	$dimg = imagecreatetruecolor($nw, $nh);
	$wm = $w/$nw;
	$hm = $h/$nh;
	$h_height = $nh/2;
	$w_height = $nw/2;
	if($w> $h) {
		$adjusted_width = $w / $hm;
		$half_width = $adjusted_width / 2;
		$int_width = $half_width - $w_height;
		imagecopyresampled($dimg,$simg,-$int_width,0,0,0,$adjusted_width,$nh,$w,$h);
	} elseif(($w <$h) || ($w == $h)) {
		$adjusted_height = $h / $wm;
		$half_height = $adjusted_height / 2;
		$int_height = $half_height - $h_height;
		imagecopyresampled($dimg,$simg,0,-$int_height,0,0,$nw,$adjusted_height,$w,$h);
	} else {
		imagecopyresampled($dimg,$simg,0,0,0,0,$nw,$nh,$w,$h);
	}
	imagejpeg($dimg,$dest,100);
} 


function resizeImageUpload($pathSrc="",$fileName,$pathDest="",$ReName="thumb-",$width=150){
	if($fileName) { //เช็คว่ามีการอัปรูป
		$images		= $pathSrc."/".$fileName;
		$size		= getimagesize($images);
		if($size[2] == 1) {
			$images_orig = imagecreatefromgif($images); //resize รูปประเภท GIF
			$pathImage=$pathDest."/".$ReName;
			//$rename = $ReName.".gif";
			$rename = $ReName;
		} else if($size[2] == 2) {
			$images_orig = imagecreatefromjpeg($images); //resize รูปประเภท JPEG
			$pathImage=$pathDest."/".$ReName;
			//$rename = $ReName.".jpg";
			$rename = $ReName;
		}
		
		$photoX = imagesx($images_orig);
		$photoY = imagesy($images_orig);

		//-- สูตรเดิม หาความกว้าง
		//$width = round($height*$size[0]/$size[1]); //ขนาดความกว้่างคำนวนเพื่อความสมส่วนของรูป

		//--สูตรตาตั้ม หาความสูง
		$height = $photoY * ($width/$photoX);
		$images_fin = imagecreatetruecolor($width, $height);
		imagecopyresized($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
		imagejpeg($images_fin, $pathImage,100); //ชื่อไฟล์ใหม่
		imagedestroy($images_orig);
		imagedestroy($images_fin);
	} 
	return $rename;
}

function resizeThumbnailImage($thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale){
	list($imagewidth, $imageheight, $imageType) = getimagesize($image);
	$imageType = image_type_to_mime_type($imageType);
	
	$newImageWidth = ceil($width * $scale);
	$newImageHeight = ceil($height * $scale);
	$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
	switch($imageType) {
		case "image/gif":
			$source=imagecreatefromgif($image); 
			break;
	    case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
			$source=imagecreatefromjpeg($image); 
			break;
	    case "image/png":
		case "image/x-png":
			$source=imagecreatefrompng($image); 
			break;
  	}
	imagecopyresampled($newImage,$source,0,0,$start_width,$start_height,$newImageWidth,$newImageHeight,$width,$height);
	switch($imageType) {
		case "image/gif":
	  		imagegif($newImage,$thumb_image_name); 
			break;
      	case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
	  		imagejpeg($newImage,$thumb_image_name,90); 
			break;
		case "image/png":
		case "image/x-png":
			imagepng($newImage,$thumb_image_name);  
			break;
    }
	chmod($thumb_image_name, 0777);
	return $thumb_image_name;
}

function getFileNameResize($fileName){
	return preg_replace("/photo/","thumb", $fileName);
}


function get_age($strBirthDay) {
	# yyyy-mm-dd
	$strNowYear = date("Y");
	list ($strBirthYear) = split ("-",$strBirthDay);
	$intAge = $strNowYear - $strBirthYear;
	return $intAge;
}



function is_email ($email) {
	return eregi("[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
}


function format_date_time ($str,$strLanguage="en")
{
	# yyyy-mm-dd hh:mm:ss --> dd/mm/yyyy hh:mm

	$str	=	preg_replace("/ |:|-/", "", $str);
	if ($str == "") { 
		return ""; 
	}

	$y	=	substr($str,0,4);
	$m	=	substr($str,4,2);
	$d	=	substr($str,6,2);
	$hr	=	substr($str,8,2);
	$min	=	substr($str,10,2);
	
	if ($strLanguage == "th") { 
		$y	+=	543;
	}

	if (strlen($str) == 8) { 
		return "$d/$m/$y";
	}
	else if (strlen($str) == 14) { 
		return "$d/$m/$y $hr:$min";
	}
	else { 
		return "";
	}
}

function datetime_to_array($str) {
	# yyyy-mm-dd hh:mm:ss --> dd/mm/yyyy hh:mm

	$str	=	preg_replace("/ |:|-/", "", $str);
	if ($str == "") { 
		return ""; 
	}
	$y		=	substr($str,0,4);
	$m		=	substr($str,4,2);
	$d		=	substr($str,6,2);
	$hr		=	substr($str,8,2);
	$min	=	substr($str,10,2);
	$sec	=	substr($str,12,2);

	return array($y,$m,$d,$hr,$min,$sec);
}

function formatDateTime($strString,$strLanguage="En") {
	# yyyy-mm-dd hh:mm:ss --> dd/mm/yyyy hh:mm

	$strString	=	preg_replace("/ |:|-/", "", $strString);
	if ($strString == "") { 
		return ""; 
	}

	$y		=	substr($strString,0,4);
	$m		=	substr($strString,4,2);
	$d		=	substr($strString,6,2);
	$hr		=	substr($strString,8,2);
	$min	=	substr($strString,10,2);
	
	if ($strLanguage == "Th") { 
		$y	+=	543;
	}

	if (strlen($strString) == 8) { 
		return "$d/$m/$y";
	}
	else if (strlen($strString) == 14) { 
		return "$d/$m/$y $hr:$min";
	}
	else { 
		return "";
	}
}

function YYYYMMDDHHMMSS2DDMMYYYYHHMM($strString) {
	return formatDateTime($strString);
}


function YYYYMMDDHHMMSS2DDMMYYYYHHMMArray($strString) {
	return dateTimeToArray($strString);
}

function DDMMYYYY2YYYYMMDD($strString) {
	#  dd/mm/yyyy --> yyyy-mm-dd
	$strString	=	preg_replace("/ |:|-|\//", "", $strString);
	
	if ($strString == "00000000" || $strString == "" || strlen($strString) != 8) { 
		return ""; 
	}
	
	$d		=	substr($strString,0,2);
	$m		=	substr($strString,2,2);
	$y		=	substr($strString,4,4);

	return "$y-$m-$d";
}
function MMDDYYYYFull($str,$lang="th"){
	$str	=	preg_replace("/ |:|-/", "", $str);
	if ($str == "") { 
		return ""; 
	}
	$y		=	substr($str,0,4);
	$m		=	substr($str,4,2);
	$d		=	substr($str,6,2);
	$hr		=	substr($str,8,2);
	$min	=	substr($str,10,2);
	return	month_name($m,$lang)." ".$d.",$y $hr:$min";
}


function month_name($month,$lang="th",$is_short_format=FALSE) {
	global $month_end_day;
	if (strtolower($lang) == "th") {
		switch ($month) {
		case 1  : $month_name = "มกราคม";	$month_short_name = "ม.ค."; $month_end_day = 31; break;
		case 2  : $month_name = "กุมภาพันธ์";	$month_short_name = "ก.พ."; $month_end_day = 29; break;
		case 3  : $month_name = "มีนาคม";	$month_short_name = "มี.ค."; $month_end_day = 31; break;
		case 4  : $month_name = "เมษายน";	$month_short_name = "เม.ย."; $month_end_day = 30; break;
		case 5  : $month_name = "พฤษภาคม";	$month_short_name = "พ.ค."; $month_end_day = 31; break;
		case 6  : $month_name = "มิถุนายน";	$month_short_name = "มิ.ย."; $month_end_day = 30; break;
		case 7  : $month_name = "กรกฏาคม";	$month_short_name = "ก.ค."; $month_end_day = 31; break;
		case 8  : $month_name = "สิงหาคม";	$month_short_name = "ส.ค."; $month_end_day = 31; break;
		case 9  : $month_name = "กันยายน";	$month_short_name = "ก.ย."; $month_end_day = 30; break;
		case 10 : $month_name = "ตุลาคม";	$month_short_name = "ต.ค."; $month_end_day = 31; break;
		case 11 : $month_name = "พฤศจิกายน";	$month_short_name = "พ.ย."; $month_end_day = 30; break;
		case 12 : $month_name = "ธันวาคม";	$month_short_name = "ธ.ค."; $month_end_day = 31; break;
		}
	}
	else {
		switch ($month) {
		case 1  : $month_name = "January";	$month_short_name = "Jan"; $month_end_day = 31; break;
		case 2  : $month_name = "February";	$month_short_name = "Feb"; $month_end_day = 29; break;
		case 3  : $month_name = "March";	$month_short_name = "Mar"; $month_end_day = 31; break;
		case 4  : $month_name = "April";	$month_short_name = "Apr"; $month_end_day = 30; break;
		case 5  : $month_name = "May";		$month_short_name = "May"; $month_end_day = 31; break;
		case 6  : $month_name = "June";		$month_short_name = "Jun"; $month_end_day = 30; break;
		case 7  : $month_name = "July";		$month_short_name = "Jul"; $month_end_day = 31; break;
		case 8  : $month_name = "August";	$month_short_name = "Aug"; $month_end_day = 31; break;
		case 9  : $month_name = "September";	$month_short_name = "Sep"; $month_end_day = 30; break;
		case 10 : $month_name = "October";	$month_short_name = "Oct"; $month_end_day = 31; break;
		case 11 : $month_name = "November";	$month_short_name = "Nov"; $month_end_day = 30; break;
		case 12 : $month_name = "December";	$month_short_name = "Dec"; $month_end_day = 31; break;	
		}
	}

	if ($is_short_format == TRUE) {
		return $month_short_name;
	}
	else {
		return $month_name;
	}
}



function send_email($to_name,$to_email,$from_name,$from_email,$subject,$body,$is_html=true,$attachs="") {

	global $strCfgSmtp;
	global $strCfgSmtpHost;
	global $strCfgSmtpSMTPAuth;
	global $strCfgSmtpUsername;
	global $strCfgSmtpPassword;
	global $strCfgMainUrl;
	global $strCfgDocumentRoot;

	require_once "$strCfgDocumentRoot/module/class.phpmailer.php";

	$mail = new PHPMailer();	
	if (is_array($attachs)) { 
		foreach (array_keys($attachs) as $key) {
			$path		=	$attachs[$key][0];
			$name		=	$attachs[$key][1];
			$encoding	=	$attachs[$key][2];
			$type		=	$attachs[$key][3];
			
			$mail->AddAttachment($path, $name, $encoding, $type);
		}
	}
	if ($strCfgSmtp) { 
		$mail->Issmtp(); // telling the class to use smtp
		$mail->Host	=	$strCfgSmtpHost;
		$mail->smtpAuth	=	$strCfgSmtpSMTPAuth;
		$mail->Username	=	$strCfgSmtpUsername;
		$mail->Password	=	$strCfgSmtpPassword;
	}

	$mail->From = $from_email;
	$mail->FromName = $from_name;
	$arr_to_email=explode(",",$to_email);
	$arr_to_name=explode(",",$to_name);
	for($i=0;$i<count($arr_to_email);$i++){
		
		$mail->AddAddress($arr_to_email[$i],$arr_to_name[$i]);
	}

	$mail->IsHtml($is_html);
	$mail->CharSet = "UTF-8";
	$mail->Subject = $subject;
	$mail->Body = $body;
	
	$mail->Send();
	#if(!$mail->Send()) {
	#   echo "Message was not sent";
	#   echo "Mailer Error: " . $mail->ErrorInfo;
	#   return False;
	#}
	#else {
		#echo "Message was sent";
		return True;
	#}
}

function get_ip() {
	if (isset($_SERVER)) {
		if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}
		else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
	}
	else {
		if (getenv('HTTP_X_FORWARDED_FOR')) {
			$ip = getenv('HTTP_X_FORWARDED_FOR');
		}
		elseif (getenv('HTTP_CLIENT_IP')) {
			$ip = getenv('HTTP_CLIENT_IP');
		}
		else {
			$ip = getenv('REMOTE_ADDR');
		}
	}

	return $ip;
}

/*echo parse_youtube_url('http://youtu.be/zc0s358b3Ys','hqthumb'); //return http://i1.ytimg.com/vi/zc0s358b3Ys/hqdefault.jpg
echo parse_youtube_url('http://www.youtube.com/watch?v=zc0s358b3Ys','embed'); //return embed code (iframe) */
function parse_youtube_url($url,$return='embed',$width='',$height='',$rel=0){ 
    $urls = parse_url($url); 
     
    //expect url is http://youtu.be/abcd, where abcd is video iD 
    if($urls['host'] == 'youtu.be'){  
        $id = ltrim($urls['path'],'/'); 
    } 
    //expect  url is http://www.youtube.com/embed/abcd 
    else if(strpos($urls['path'],'embed') == 1){  
        $id = end(explode('/',$urls['path'])); 
    } 
     //expect url is abcd only 
    else if(strpos($url,'/')===false){ 
        $id = $url; 
    } 
    //expect url is http://www.youtube.com/watch?v=abcd 
    else{ 
        parse_str($urls['query']); 
        $id = $v; 
    } 
    //return embed iframe 
    if($return == 'embed'){ 
        return '<iframe width="'.($width?$width:560).'" height="'.($height?$height:349).'" src="http://www.youtube.com/embed/'.$id.'?rel='.$rel.'" frameborder="0" allowfullscreen></iframe>'; 
    } 
    //return normal thumb 
    else if($return == 'thumb'){ 
        return 'http://i1.ytimg.com/vi/'.$id.'/default.jpg'; 
    } 
    //return hqthumb 
    else if($return == 'hqthumb'){ 
        return 'http://i1.ytimg.com/vi/'.$id.'/hqdefault.jpg'; 
    } 
    // else return id 
    else{ 
        return $id; 
    } 
} 

?>