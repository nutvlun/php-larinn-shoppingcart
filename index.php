<?php
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*

Program     : 
Description : 
Programmer  : Thrinai Chankong

Email       : thrinai@hotmail.com
Website     : http://www.plapayoon.com

Date        : 
Modify log  : 

*/
/*------------------------------------------------------------------*/
/*- Require --------------------------------------------------------*/

include("module/SiXhEaD.Template.php");
include("module/SiXhEaD.Pagination.php");
include("module/_config.php");
include("module/_module.php");

/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/

$tp_index	 =	"_tp_index.html";

/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/

$Today	 =	 date("Y-m-d");
$strLogInBar	=	 CheckLogin();

/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/



/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/
$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
if (!$conn) {die('Not connected : ' . mysql_error());}
// make foo the current db
$db_selected = mysql_select_db($strCfgDbName, $conn);
if (!$db_selected) { die ('Can\'t use $strCfgDbName : ' . mysql_error());}
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");
$tp = new Template($tp_index);

/*------------------------- News --------------------------*/

$sql ="SELECT `NewsId`,`Mark`,`ThTitle`,`ThShortDetail`,`File1`,ActivityDate FROM $strCfgDbTableNews WHERE (IsDelete='N' AND Active='Y') AND  ActivityDate<='$Today' ORDER BY  Mark DESC,LineNo ASC,`NewsId` DESC  LIMIT 0,1";
$result=mysql_query($sql);
if(mysql_num_rows($result)>0){
	$tp->block("BlockNews");
	while($row=mysql_fetch_assoc($result)){
		$Id					=	$row["NewsId"];
		$Title				=	$row["ThTitle"];
		$ShortDetail			=	$row["ThShortDetail"];
		$File1					=	$row["File1"];
	
	$ImgNews = <<<img
module/phpThumb/phpThumb.php?src=../../content/news/$File1&w=$strCfgNewsThumbWidth&h=$strCfgNewsThumbHeight&zc=1&sx=0
img;
	
	}
	$tp->apply();
}
/*-------------------------// News --------------------------*/
/*------------------------- Keyvisual --------------------------*/

$sql="SELECT `KeyId`,`File1`,`Label`,`Url` FROM $strCfgDbTableKeyvisual WHERE  (IsDelete='N' AND Active='Y') ORDER BY LineNo ASC,`KeyId` DESC";
$result	=	mysql_query($sql);
$tp->block("Keyvisual");
while($row=mysql_fetch_assoc($result)){
	$Url	=	$row[Url];
	$Label	=	$row[Label];
	$File1	=	$row[File1];
	$ImgKeyvisual ="module/phpThumb/phpThumb.php?src=../../content/keyvisual/$File1&w=$strCfgKeyVisualWidth&h=$strCfgKeyVisualHeight&zc=1&sx=0";
	$ImgKeyvisualThumb ="module/phpThumb/phpThumb.php?src=../../content/keyvisual/$File1&w=50&h=50&zc=1&sx=0";
	$tp->apply();
}
/*-------------------------// Keyvisual --------------------------*/

/*------------------------- Product --------------------------*/
$sql="SELECT `ProductCode`,`ProductName`,`Price`,`AfterDisPrice`,`File1` FROM $strCfgDbTableProduct WHERE (IsDelete='N' AND Active='Y') AND  `NewProduct`='Y' GROUP BY `ProductCode` ORDER BY `LineNo` ASC,`ProductId` ASC";
$result	=	mysql_query($sql);

$tp->block("ListData");
$cols=5;
$i=0;

while($row=mysql_fetch_assoc($result)){
	$ProductCode	= $row[ProductCode];
	$ProductName	=	$row[ProductName];
	$Price			=	$row[Price];
	$AfterDisPrice	=	$row[AfterDisPrice];
	if($AfterDisPrice!="0.00")$Price="<span style=\"text-decoration:line-through;\"  class=\"text_13b_1\">".$Price." thb</span> &nbsp;<b>$AfterDisPrice</b>";
	$File1	=	$row[File1];
	$ImgProduct = <<<img
		module/phpThumb/phpThumb.php?src=../../content/product/$File1&w=$strCfgProductThumbWidth&h=$strCfgProductThumbHeight&zc=1&sx=0
img;
	$tp->apply();
	$i++;
}
$divBlank	=	"";
for($k=0;$k<($cols-intval($i%$cols));$k++){
	$divBlank	.=	"<li>&nbsp;</li>";
}
/*-------------------------// Product --------------------------*/
mysql_free_result($result);
mysql_close($conn);
$tp->Display();
exit;

/*------------------------------------------------------------------*/
?>