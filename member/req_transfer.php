<?php
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*

Program     : 
Description : 
Programmer  : Thrinai Chankong

Email       : thrinai@hotmail.com
Website     : http://www.hotelinchaam.com

Date        : 
Modify log  : 

*/
/*------------------------------------------------------------------*/
/*- Require --------------------------------------------------------*/

include("../module/SiXhEaD.Template.php");
include("../module/SiXhEaD.Pagination.php");
include("../module/_config.php");
include("../module/_module.php");

/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/

$tp_index	 =	"_tp_req_transfer.html";
$tp_complete	 =	"_tp_transfer_complete.html";

/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/

$strLogInBar	=	 CheckLogin();

/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/
$su_member_id	=	$_SESSION["su_member_id"];
$Send		=	$_REQUEST['Send'];
$Name		=	$_REQUEST['Name'];
$Email		=	$_REQUEST['Email'];
$InvNo		=	$_REQUEST['InvNo'];
$Tel		=	$_REQUEST['Tel'];
$Bank		=	$_REQUEST['Bank'];
$Money		=	$_REQUEST['Money'];
$TrnDate	=	$_REQUEST['TrnDate'];
$TrnTime	=	$_REQUEST['TrnTime'];
$captcha	=	$_REQUEST['captcha'];

/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/
$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
if (!$conn) {die('Not connected : ' . mysql_error());}
// make foo the current db
$db_selected = mysql_select_db($strCfgDbName, $conn);
if (!$db_selected) { die ('Can\'t use $strCfgDbName : ' . mysql_error());}
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");
if($Send=="Y"){
	if($captcha != $_SESSION[siSCContactus]){
		$tp = new Template($tp_index);
		$tp->block("Error");
		$error="* กรุณาใส่ข้อมูลดังภาพที่แสดง!!!";
		$tp->apply();
		$tp->Display();
		exit;
	}

$Subject	=	"แจ้งโอนเงิน";
$Message	=	"Invoice No : $InvNo<BR>";
$Message	.=	"โอนเงินเข้าธนาคาร : $Bank<BR>";
$Message	.=	"จำนวนเงิน : ".number_format($Money,2, '.', ',');
$Message	.= "<BR>วันที่ $TrnDate  เวลา $TrnTime   ";

$sql="INSERT INTO $strCfgDbTableContactUs(`IsDelete`,`MemberId`,`Name`,`Email`,`Tel`,Subject,`Message`,`AddDate`)VALUES('N','$su_member_id','$Name','$Email','$Tel','$Subject','$Message',Now())";
mysql_query($sql);
$Id=mysql_insert_id();

$from_name=$Name;
$Title	=	$Subject;
$body	 =	"ชื่อผู้ติดต่อ : ".$Name."<BR>";
$body	 .=	"Email : ".$Email."<BR>";
$body	 .=	"เบอร์โทรศัพท์ : ".$Tel."<BR>";
$body	 .=	"ข้อความ :<BR>";
$body	 .=	"----------------------------------------------------<BR>";
$body	 .=	$Message;
$body	 .=	"<BR>----------------------------------------------------<BR>";

send_email('',$strCfgEmailContactUs,$from_name,$Email,$Title,$body,true);

mysql_close($conn);
$tp = new Template($tp_complete);
$tp->Display();
exit;

}
$sql	=	"SELECT * FROM $strCfgDbTableBank ORDER BY BankId";
$result	=	mysql_query($sql);
$tp = new Template($tp_index);
$tp->block("bank");
while($row=mysql_fetch_assoc($result)){
	$BankId	=	$row[BankId];
	$BankName=	$row[BankName];
	
	$tp->apply();
}
$tp->Display();
exit;


/*------------------------------------------------------------------*/
?>