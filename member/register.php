<?
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*

Program     : 
Description : 
Programmer  : ตรินัยน์ จันทร์คง

Individual
Email       : thrinai@hotmail.com
Website     : 


Office
Email       : thrinai@digithais.com
Website     : http://www.digithais.com

Date        : 02-07-2009
Modify log  : 

*/
/*------------------------------------------------------------------*/
/*- Include Library --------------------------------------------------------*/
include("../module/SiXhEaD.Template.php");
include("../module/SiXhEaD.Pagination.php");
include("../module/_config.php");
include("../module/_module.php");

/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/


$tp_index			=	"_tp_register.html";
$tp_complete		=	"_tp_register_complete.html";


/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/

$Today	 =	 date("Y-m-d");

$strLogInBar	=	 CheckLogin();
/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/


$Send		=	$_REQUEST["Send"];
$Email		=	$_REQUEST[Email];
$Pwd		=	$_REQUEST[Pwd];
$RePwd		=	$_REQUEST[RePwd];
$Fname		=	$_REQUEST[Fname];
$Lname		=	$_REQUEST[Lname];
$Gender		=	$_REQUEST[Gender];
$Birthday	=	DDMMYYYY2YYYYMMDD($_REQUEST[Birthday]);
$Address	=	$_REQUEST[Address];
$Province	=	$_REQUEST[Province];
$Zipcode	=	$_REQUEST[Zipcode];
$Phone		=	$_REQUEST[Phone];


/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/
if($Send=="Y"){
	$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
	if (!$conn) {die('Not connected : ' . mysql_error());}
	// make foo the current db
	$db_selected = mysql_select_db($strCfgDbName, $conn);
	if (!$db_selected) { die ('Can\'t use $strCfgDbName : ' . mysql_error());}
	mysql_query("SET NAMES UTF8");
	mysql_query("SET character_set_results=UTF8");
	$sql	=	"SELECT `Email` FROM $strCfgDbTableMember WHERE `Email`='$Email' AND `IsDelete`='N'";
	$result	=	mysql_query($sql);
	$num_rows = mysql_num_rows($result);
	if($num_rows>0){
		$error	=	"E-mail นี้มีอยู่ในระบบแล้วค่ะ";
		$tp = new Template($tp_index);
		$tp->Display();
		mysql_free_result($result);
		mysql_close($conn);
		exit;
	}

	
	$sql="Insert Into $strCfgDbTableMember (`IsDelete`,`Email`,`Pwd`,`Fname`,`Lname`,`Gender`,`Birthday`,`Address`,`Province`,`Zipcode`,`Phone`,`Active`,`AddDate`)VALUES ('N','$Email','$Pwd','$Fname','$Lname','$Gender','$Birthday','$Address','$Province','$Zipcode','$Phone','Y',Now())";

	mysql_query($sql);
	mysql_close($conn);
	$tp = new Template($tp_complete);
	$tp->Display();
	exit;
}
$tp = new Template($tp_index);
$tp->Display();
exit;

?>