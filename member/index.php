<?php
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*

Program     : 
Description : 
Programmer  : Thrinai Chankong

Email       : thrinai@hotmail.com
Website     : http://www.hotelinchaam.com

Date        : 
Modify log  : 

*/
/*------------------------------------------------------------------*/
/*- Require --------------------------------------------------------*/

include("../module/SiXhEaD.Template.php");
include("../module/SiXhEaD.Pagination.php");
include("../module/_config.php");
include("../module/_module.php");

/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/

$tp_index	 =	"_tp_index.html";

/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/

authen_user ();
$strLogInBar	=	 CheckLogin();

/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/

$su_member_id	=	$_SESSION["su_member_id"];

/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/
$tp = new Template($tp_index);
$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
if (!$conn) {die('Not connected : ' . mysql_error());}
// make foo the current db
$db_selected = mysql_select_db($strCfgDbName, $conn);
if (!$db_selected) { die ('Can\'t use $strCfgDbName : ' . mysql_error());}
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");
//- จัด Range ใหม่

if(!$LimitPage){$LimitPage=10;}
//- Condition
$WHERE = " AND `MemberId`='$su_member_id'";
$order_by = " `InvHId` DESC ";



$sp	= New Pagination();
$sp->db_type			=	"MySQL";
$sp->db_table			=	$strCfgDbTableInvH;
$sp->order_by			=	$order_by; 
$sp->primary_key	=	"`InvHId`";
$sp->select			=	"`InvHId`,`InvNo`,`MemberId`,`PayBy`,`Total`,`Status`,`PayTime`,`ShipTime`,`AddDate`";
$sp->custom_sql		=	" WHERE  IsDelete='N' $WHERE  " ; # WHERE, JOIN
$sp->custom_param	=	"&LimitPage=$LimitPage&cate=$cate&searchq=$searchq";
$sp->per_page		=	$LimitPage;
$sp->order_param	=	"order"; # $_GET["order"];
$sp->page_param	=	"page"; # $_REQUEST["p"];
$sp->page_prev		=	"&#8249; Prev"; //
$sp->page_next		=	"Next &#8250;";
$sp->page_first		=	"&laquo; First";
$sp->page_last		=	"Last &raquo;";
$sp->sign_left			=	"";
$sp->sign_right		=	" |";
$sp->sign_trim		=	FALSE;
$sp->style				=	1;
$sp->start();

$intRecordAll	=	$sp->get_record_all();
$sql				=	$sp->get_sql();
$strPageLink	=	$sp->get_page_link();
$intPageAll		=	$sp->get_all_page();

$bg_color = "";
$result = mysql_query($sql); //echo $sql;
if (!$result) { echo "$sql"; die('Invalid query: ' . mysql_error()); }
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");



$tp->block("DATA");

$i=0;
$strTbl	=	"";
while($row = mysql_fetch_array($result)) {
	$Id					=	$row["InvHId"];
	
	$InvNo				=	$row["InvNo"];
	$PayBy				=	($row["PayBy"]=="bank")?"Bank":"PayPal";
	
	$Total				=	number_format($row["Total"],2, '.', ',');

	switch ($row["Status"]) {
    case "wait_pay":
       $Status		= "รอการชำระเงิน";
        break;
    case "pay_completed":
         $Status	="ชำระเงินเรียบร้อยแล้ว";
        break;
    case "wait_ship":
        $Status		= "กำลังจัดส่ง";
        break;
	 case "shiped":
         $Status	= "จัดส่งแล้ว";
        break;
	case "cancel":
         $Status	= "ยกเลิกรายการ";
        break;
	}
	$AddDate			=	YYYYMMDDHHMMSS2DDMMYYYYHHMM($row["AddDate"]);
	
	
	
	$tp->apply();
	$i++;
}

mysql_free_result($result);
mysql_close($conn);
$tp->Display();
exit;


/*------------------------------------------------------------------*/
?>