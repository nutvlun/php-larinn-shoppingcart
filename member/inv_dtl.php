<?php
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*

Program     : 
Description : 
Programmer  : Thrinai Chankong

Email       : thrinai@hotmail.com
Website     : http://www.hotelinchaam.com

Date        : 
Modify log  : 

*/
/*------------------------------------------------------------------*/
/*- Require --------------------------------------------------------*/

include("../module/SiXhEaD.Template.php");
include("../module/SiXhEaD.Pagination.php");
include("../module/_config.php");
include("../module/_module.php");

/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/

$tp_index	 =	"_tp_inv_dtl.html";

/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/

authen_user ();


/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/

$su_member_id	=	$_SESSION["su_member_id"];
$Id		=	$_REQUEST['Id'];

/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/
$tp = new Template($tp_index);
$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
if (!$conn) {die('Not connected : ' . mysql_error());}
// make foo the current db
$db_selected = mysql_select_db($strCfgDbName, $conn);
if (!$db_selected) { die ('Can\'t use $strCfgDbName : ' . mysql_error());}
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");

$sql="SELECT $strCfgDbTableInvH.`InvNo` As `InvNo`,$strCfgDbTableInvH.`Status` As `Status`,$strCfgDbTableInvH.`Total` As `Total`,$strCfgDbTableInvH.`AddDate` As `AddDate`,$strCfgDbTableMember.`Email` As `Email`,$strCfgDbTableMember.`Fname` As `Fname`,$strCfgDbTableMember.`Lname` As `Lname`,$strCfgDbTableMember.`Address` As `Address`,$strCfgDbTableMember.`Province` As `Province`,$strCfgDbTableMember.`Zipcode` As `Zipcode`,$strCfgDbTableMember.`Phone` As `Phone` FROM $strCfgDbTableInvH INNER JOIN $strCfgDbTableMember ON $strCfgDbTableMember.`MemberId`=$strCfgDbTableInvH.`MemberId` WHERE $strCfgDbTableInvH.`InvHId`='$Id' AND $strCfgDbTableInvH.`IsDelete`='N' ";
$result	=	mysql_query($sql);
@extract(mysql_fetch_assoc($result));
$AddDate			=	YYYYMMDDHHMMSS2DDMMYYYYHHMM($AddDate);
$Total				=	number_format($Total,2, '.', ',');

$sql="SELECT $strCfgDbTableInvDtl.`InvDtlId` As `InvDtlId`,$strCfgDbTableInvDtl.`ProductId` As `ProductId`,$strCfgDbTableInvDtl.`ProductCode` As `ProductCode`,$strCfgDbTableInvDtl.`ProductName` As `ProductName`,$strCfgDbTableInvDtl.`Size` As `Size`,$strCfgDbTableInvDtl.`ColorId` As `ColorId`,$strCfgDbTableInvDtl.`Qty` As `Qty`,$strCfgDbTableInvDtl.`UnitPrice` As `UnitPrice`,$strCfgDbTableInvDtl.`SubTotal` As `SubTotal`,$strCfgDbTableProduct.`File1` As `File1`  FROM $strCfgDbTableInvDtl  Inner JOIN $strCfgDbTableProduct ON $strCfgDbTableProduct.`ProductId`=$strCfgDbTableInvDtl.`ProductId` WHERE $strCfgDbTableInvDtl.`InvHId`='$Id' ORDER BY $strCfgDbTableInvDtl.`InvDtlId` ASC";
$result	=	mysql_query($sql);
$tp->block("data");
while($row=mysql_fetch_assoc($result)){
	$InvDtlId	=	$row["InvDtlId"];
	$ProductId	=	$row["ProductId"];
	$ProductCode	=	$row["ProductCode"];
	$ProductName	=	$row["ProductName"];
	$Size	=	$row["Size"];
	$ColorId	=	$row["ColorId"];
	$Qty	=	$row["Qty"];
	$UnitPrice	=	$row["UnitPrice"];
	$SubTotal	=	$row["SubTotal"];
	$File1	=	$row["File1"];
	$Img=	"../module/phpThumb/phpThumb.php?src=../../content/product/$File1&w=$strCfgProductThumbWidth&h=$strCfgProductThumbHeight&zc=1&sx=0";
	$tp->apply();
}
mysql_close($conn);
$tp->Display();
exit;


/*------------------------------------------------------------------*/
?>