-- phpMyAdmin SQL Dump
-- version 2.6.0-pl2
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Jun 24, 2013 at 01:05 AM
-- Server version: 5.1.30
-- PHP Version: 5.2.7
-- 
-- Database: `larinn_db`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `admin`
-- 

CREATE TABLE `admin` (
  `Id` int(2) NOT NULL AUTO_INCREMENT,
  `Username` varchar(30) NOT NULL DEFAULT '',
  `Password` varchar(30) NOT NULL DEFAULT '',
  `Fname` varchar(100) DEFAULT NULL,
  `Lname` varchar(100) DEFAULT NULL,
  `AdminType` enum('SuperAdmin','Admin') NOT NULL DEFAULT 'Admin',
  `Email` varchar(100) NOT NULL DEFAULT '',
  `AddDate` datetime DEFAULT NULL,
  `Active` enum('N','Y') NOT NULL DEFAULT 'Y',
  `UpdateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Username` (`Username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `admin`
-- 

INSERT INTO `admin` VALUES (1, 'admin', 'password', 'Administrator', NULL, 'SuperAdmin', '', '2011-05-24 12:31:24', 'Y', 0x323031312d30352d32342031313a33313a3234);

-- --------------------------------------------------------

-- 
-- Table structure for table `bank`
-- 

CREATE TABLE `bank` (
  `BankId` int(11) NOT NULL AUTO_INCREMENT,
  `BankName` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `BankBranch` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `AccountType` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `AccountName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `AccountNo` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `File1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`BankId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `bank`
-- 

INSERT INTO `bank` VALUES (1, 'ธนาคารกสิกรไทย', 'สยามสแควร์', 'ออมทรัพย์', 'Larinn', '2-5456-51125-1', 'cart_01_r1_c4.png');
INSERT INTO `bank` VALUES (2, 'ธนาคารไทยพาณิชย์', 'สยามสแควร์', 'ออมทรัพย์', 'Larinn', '2-5556-51125-7', 'cart_01_r8_c3.png');

-- --------------------------------------------------------

-- 
-- Table structure for table `category`
-- 

CREATE TABLE `category` (
  `CateId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IsDelete` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `LineNo` int(10) unsigned NOT NULL DEFAULT '999999',
  `CateName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Active` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `AddDate` datetime NOT NULL,
  `UpdateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CateId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `category`
-- 

INSERT INTO `category` VALUES (1, 'N', 999999, 'SHOES', 'Y', '2013-06-13 00:27:23', 0x323031332d30362d31332030303a32373a3233);

-- --------------------------------------------------------

-- 
-- Table structure for table `color`
-- 

CREATE TABLE `color` (
  `ColorId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IsDelete` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `LineNo` int(10) unsigned NOT NULL DEFAULT '999999',
  `ColorName` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ColorCode` varchar(7) COLLATE utf8_unicode_ci DEFAULT '000000',
  `Active` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `AddDate` datetime NOT NULL,
  `UpdateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ColorId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `color`
-- 

INSERT INTO `color` VALUES (1, 'N', 1, 'Black', '000000', 'Y', '2013-06-13 01:47:46', 0x323031332d30362d31332030323a31383a3432);
INSERT INTO `color` VALUES (2, 'N', 2, 'Red', 'ff0000', 'Y', '2013-06-13 02:18:37', 0x323031332d30362d31332030323a31383a3432);

-- --------------------------------------------------------

-- 
-- Table structure for table `contactus`
-- 

CREATE TABLE `contactus` (
  `ContactId` int(11) NOT NULL AUTO_INCREMENT,
  `IsDelete` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `MemberId` int(10) unsigned DEFAULT '0',
  `Name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Tel` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Subject` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Message` text COLLATE utf8_unicode_ci,
  `AddDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ContactId`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `contactus`
-- 

INSERT INTO `contactus` VALUES (1, 'N', 1, 'thrinai', 'thrinai@yahoo.com', '0865155907', 'แจ้งโอนเงิน', 'Invoice No : 0000001<BR>โอนเงินเข้าธนาคาร : ธนาคารกสิกรไทย<BR>จำนวนเงิน : 401.00<BR>วันที่ 23/06/2013  เวลา 10:00   ', '2013-06-24 00:02:43');

-- --------------------------------------------------------

-- 
-- Table structure for table `inv_dtl`
-- 

CREATE TABLE `inv_dtl` (
  `InvDtlId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IsDelete` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `InvHId` bigint(20) unsigned NOT NULL DEFAULT '0',
  `ProductId` int(10) unsigned NOT NULL DEFAULT '0',
  `ProductCode` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProductName` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Size` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ColorId` int(10) unsigned NOT NULL DEFAULT '0',
  `Qty` int(10) unsigned NOT NULL DEFAULT '0',
  `UnitPrice` float(12,2) NOT NULL DEFAULT '0.00',
  `SubTotal` double(20,2) NOT NULL DEFAULT '0.00',
  `Active` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `AddDate` datetime NOT NULL,
  `UpdateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`InvDtlId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `inv_dtl`
-- 

INSERT INTO `inv_dtl` VALUES (1, 'N', 1, 2, '000001', 'รองเท้าส้นสูง ยี่ห้อ Cabel รุ่น 47', '42', 2, 1, 400.00, 400.00, 'Y', '2013-06-23 00:25:02', 0x323031332d30362d32332030303a32353a3032);

-- --------------------------------------------------------

-- 
-- Table structure for table `inv_h`
-- 

CREATE TABLE `inv_h` (
  `InvHId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IsDelete` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `InvNo` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MemberId` int(10) unsigned NOT NULL DEFAULT '0',
  `ShipToName` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ShipToAddress` text COLLATE utf8_unicode_ci,
  `ShiptToPhone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ShipingPrice` float(12,2) NOT NULL DEFAULT '0.00',
  `DiscountPercent` float(5,2) NOT NULL DEFAULT '0.00',
  `DiscountPrice` float(12,2) NOT NULL DEFAULT '0.00',
  `SubTotal` double(20,2) NOT NULL DEFAULT '0.00',
  `Total` double(20,2) NOT NULL DEFAULT '0.00',
  `PayBy` enum('bank','paypal') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'bank',
  `PayTime` datetime DEFAULT NULL,
  `ShipTime` datetime DEFAULT NULL,
  `Status` enum('wait_pay','pay_completed','wait_ship','shiped','cancel') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'wait_pay',
  `AddDate` datetime NOT NULL,
  `UpdateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`InvHId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `inv_h`
-- 

INSERT INTO `inv_h` VALUES (1, 'N', '0000001', 1, NULL, NULL, NULL, 0.00, 0.00, 0.00, 0.00, 400.00, 'bank', NULL, NULL, 'wait_pay', '2013-06-23 00:25:02', 0x323031332d30362d32332032313a31393a3535);

-- --------------------------------------------------------

-- 
-- Table structure for table `keyvisual`
-- 

CREATE TABLE `keyvisual` (
  `KeyId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IsDelete` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `LineNo` int(10) unsigned NOT NULL DEFAULT '999999',
  `File1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Label` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Url` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Active` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `AddDate` datetime NOT NULL,
  `UpdateDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`KeyId`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `keyvisual`
-- 

INSERT INTO `keyvisual` VALUES (1, 'N', 999999, '1-File1.jpg', 'Label1_2', 'http://www.goal.com/th/news/4265/%E0%B8%84%E0%B8%AD%E0%B8%A5%E0%B8%B1%E0%B8%A1%E0%B8%99%E0%B9%8C/2013/06/24/4064818/Fan-View-%E0%B8%95%E0%B8%B1%E0%B8%94%E0%B9%80%E0%B8%81%E0%B8%A3%E0%B8%94%E0%B9%84%E0', 'Y', '2013-06-23 20:38:32', 0x323031332d30362d32332032303a34323a3338);
INSERT INTO `keyvisual` VALUES (2, 'N', 999999, '2-File1.jpg', 'Label', 'http://www.goal.com/th/news/4256/%E0%B8%9F%E0%B8%B8%E0%B8%95%E0%B8%9A%E0%B8%AD%E0%B8%A5%E0%B8%AD%E0%B8%B1%E0%B8%87%E0%B8%81%E0%B8%A4%E0%B8%A9/2013/06/17/4053322/%E0%B8%A1%E0%B8%B1%E0%B8%99%E0%B9%82%E0', 'Y', '2013-06-23 20:43:08', 0x323031332d30362d32332032303a34333a3038);

-- --------------------------------------------------------

-- 
-- Table structure for table `member`
-- 

CREATE TABLE `member` (
  `MemberId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IsDelete` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `Email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Pwd` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Fname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Lname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Gender` enum('M','F') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'F',
  `Birthday` date DEFAULT NULL,
  `Address` text COLLATE utf8_unicode_ci,
  `Province` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Zipcode` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Phone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Active` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `AddDate` datetime NOT NULL,
  `UpdateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MemberId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `member`
-- 

INSERT INTO `member` VALUES (1, 'N', 'thrinai@yahoo.com', '123456', 'thrinai', 'chankong', 'M', 0x313938312d30342d3135, '161/779 ซอยบุปผาสวรรค์', 'กรุงเทพ', '10700', '0865155907', 'Y', '2013-06-19 02:02:30', 0x323031332d30362d32332032323a34363a3133);

-- --------------------------------------------------------

-- 
-- Table structure for table `news`
-- 

CREATE TABLE `news` (
  `NewsId` int(11) NOT NULL AUTO_INCREMENT,
  `IsDelete` enum('N','Y') COLLATE utf8_unicode_ci DEFAULT 'N',
  `LineNo` int(11) DEFAULT '999999999',
  `Mark` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ThTitle` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ActivityDate` date DEFAULT NULL,
  `ThShortDetail` text COLLATE utf8_unicode_ci,
  `ThDetail` longtext COLLATE utf8_unicode_ci,
  `EnTitle` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EnShortDetail` text COLLATE utf8_unicode_ci,
  `EnDetail` longtext COLLATE utf8_unicode_ci,
  `File1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `File2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `File3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `File4` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `File5` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `File6` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Active` enum('N','Y') COLLATE utf8_unicode_ci DEFAULT 'N',
  `AddDate` datetime DEFAULT NULL,
  `UpdateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`NewsId`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `news`
-- 

INSERT INTO `news` VALUES (1, 'N', 999999999, 'N', 'ทดสอบ', 0x323031332d30352d3238, 'ทดสอบ short detail', '<p>[V] Fun Republic ซ่านิยม By All New Honda Scoopy i วันนี้ วีเจลูกเกด วีเจพิตต้า ต้อนรับ 2 สาว<br />\r\nเจ้าของแบรนด์ Larinn by Double P  ที่มาพร้อมรองเท้าสวยๆ เก๋ๆ หลายแบบ :) [V] Fun Republic ซ่านิยม <br />\r\nBy All New Honda Scoopy i วันนี้  วีเจลูกเกด วีเจพิตต้า ต้อนรับ 2 สาวเจ้าของแบรนด์ Larinn by <br />\r\nDouble P ที่มาพร้อมรองเท้าสวยๆ เก๋ๆ หลายแบบ :)<br />\r\n<br />\r\n[V] Fun Republic ซ่านิยม By All  New Honda Scoopy i วันนี้ วีเจลูกเกด วีเจพิตต้า ต้อนรับ 2 สาว<br />\r\nเจ้าของแบรนด์ Larinn by Double P ที่มาพร้อมรองเท้าสวยๆ เก๋ๆ หลายแบบ :) [V] Fun Republic ซ่านิยม <br />\r\nBy All New Honda Scoopy i วันนี้ วีเจลูกเกด วีเจพิตต้า ต้อนรับ 2 สาวเจ้าของแบรนด์ Larinn by <br />\r\nDouble P ที่มาพร้อมรองเท้าสวยๆ เก๋ๆ หลายแบบ :)                                                                                                                 <br />\r\n<br />\r\n<span class="text_shadow_13b_4">[V] Fun Republic ซ่านิยม By All New Honda Scoopy i วันนี้ วีเจลูกเกด วีเจพิตต้า ต้อนรับ 2 สาว<br />\r\nเจ้าของแบรนด์ Larinn by Double P ที่มาพร้อมรองเท้าสวยๆ เก๋ๆ หลายแบบ :) [V] Fun Republic ซ่านิยม <br />\r\nBy All New Honda Scoopy i วันนี้ วีเจลูกเกด วีเจพิตต้า ต้อนรับ 2 สาวเจ้าของแบรนด์ Larinn by <br />\r\nDouble P ที่มาพร้อมรองเท้าสวยๆ เก๋ๆ หลายแบบ :)</span></p>', '', '', '', '1-File1.jpg', '1-File2.jpg', '1-File3.jpg', '1-File4.jpg', '1-File5.jpg', '1-File6.jpg', 'Y', '2013-05-28 00:17:08', 0x323031332d30352d32382030303a31373a3038);

-- --------------------------------------------------------

-- 
-- Table structure for table `product`
-- 

CREATE TABLE `product` (
  `ProductId` int(11) NOT NULL AUTO_INCREMENT,
  `IsDelete` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `LineNo` int(10) unsigned NOT NULL DEFAULT '999999',
  `NewProduct` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CateId` int(10) unsigned NOT NULL DEFAULT '0',
  `SubCateId` int(10) unsigned NOT NULL DEFAULT '0',
  `ProductCode` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProductName` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Price` float(12,2) NOT NULL DEFAULT '0.00',
  `AfterDisPrice` float(12,2) NOT NULL DEFAULT '0.00',
  `Detail` longtext COLLATE utf8_unicode_ci,
  `ColorId` int(10) unsigned NOT NULL DEFAULT '0',
  `Size` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `File1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `File2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `File3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `File4` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DefaultProduct` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `Active` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `AddDate` datetime NOT NULL,
  `UpdateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ProductId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `product`
-- 

INSERT INTO `product` VALUES (1, 'N', 1, 'Y', 1, 1, '000001', 'รองเท้าส้นสูง ยี่ห้อ Cabel รุ่น 47', 400.00, 300.00, '<p>วีเจลูกเกด วีเจพิตต้า ต้อนรับ 2 สาวเจ้าของ แบรนด์ Larinn by Double P ที่มาพร้อมรองเท้าสวยๆ เก๋ๆ หลายแบบ...</p>\r\n<p>วีเจลูกเกด วีเจพิตต้า ต้อนรับ 2 สาวเจ้าของ แบรนด์ Larinn by Double P ที่มาพร้อมรองเท้าสวยๆ เก๋ๆ หลายแบบ...</p>\r\n<p>วีเจลูกเกด วีเจพิตต้า ต้อนรับ 2 สาวเจ้าของ แบรนด์ Larinn by Double P ที่มาพร้อมรองเท้าสวยๆ เก๋ๆ หลายแบบ...</p>\r\n<p>&nbsp;</p>', 1, '1|2|3|4|5|6|7|8', '1-File1.jpg', '1-File2.jpg', '1-File3.jpg', '1-File4.jpg', 'N', 'Y', '2013-06-17 00:15:38', 0x323031332d30362d32322032313a31343a3238);
INSERT INTO `product` VALUES (2, 'N', 2, 'Y', 1, 1, '000001', 'รองเท้าส้นสูง ยี่ห้อ Cabel รุ่น 47', 400.00, 0.00, '<p>วีเจลูกเกด วีเจพิตต้า ต้อนรับ 2 สาวเจ้าของ แบรนด์ Larinn by <br />\r\nDouble P ที่มาพร้อมรองเท้าสวยๆ เก๋ๆ หลายแบบ...</p>\r\n<p>วีเจลูกเกด วีเจพิตต้า ต้อนรับ 2 สาวเจ้าของ แบรนด์ Larinn by <br />\r\nDouble P ที่มาพร้อมรองเท้าสวยๆ เก๋ๆ หลายแบบ...</p>\r\n', 2, '1|7|8', '2-File1.jpg', '2-File2.jpg', '2-File3.jpg', '2-File4.jpg', 'N', 'Y', '2013-06-17 01:35:03', 0x323031332d30362d32322032313a31343a3334);

-- --------------------------------------------------------

-- 
-- Table structure for table `size`
-- 

CREATE TABLE `size` (
  `SizeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IsDelete` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `LineNo` int(10) unsigned NOT NULL DEFAULT '999999',
  `Size` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Active` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `AddDate` datetime NOT NULL,
  `UpdateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`SizeId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

-- 
-- Dumping data for table `size`
-- 

INSERT INTO `size` VALUES (1, 'N', 1, '35', 'Y', '2013-06-13 01:05:49', 0x323031332d30362d31332030313a30383a3334);
INSERT INTO `size` VALUES (2, 'N', 2, '36', 'Y', '2013-06-13 01:06:05', 0x323031332d30362d31332030313a30383a3230);
INSERT INTO `size` VALUES (3, 'N', 3, '37', 'Y', '2013-06-13 01:06:12', 0x323031332d30362d31332030313a30383a3230);
INSERT INTO `size` VALUES (4, 'N', 4, '38', 'Y', '2013-06-13 01:06:20', 0x323031332d30362d31332030313a30383a3230);
INSERT INTO `size` VALUES (5, 'N', 5, '39', 'Y', '2013-06-13 01:06:25', 0x323031332d30362d31332030313a30383a3230);
INSERT INTO `size` VALUES (6, 'N', 6, '40', 'Y', '2013-06-13 01:07:49', 0x323031332d30362d31332030313a30383a3230);
INSERT INTO `size` VALUES (7, 'N', 7, '41', 'Y', '2013-06-13 01:07:55', 0x323031332d30362d31332030313a30383a3230);
INSERT INTO `size` VALUES (8, 'N', 8, '42', 'Y', '2013-06-13 01:08:01', 0x323031332d30362d31332030313a30383a3230);

-- --------------------------------------------------------

-- 
-- Table structure for table `sub_category`
-- 

CREATE TABLE `sub_category` (
  `SubCateId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IsDelete` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `LineNo` int(10) unsigned NOT NULL DEFAULT '999999',
  `SubCateName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CateId` int(10) unsigned NOT NULL DEFAULT '0',
  `Active` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `AddDate` datetime NOT NULL,
  `UpdateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`SubCateId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `sub_category`
-- 

INSERT INTO `sub_category` VALUES (1, 'N', 1, 'รองเท้าส้นสูง', 1, 'Y', '2013-06-13 00:39:26', 0x323031332d30362d31332030303a34313a3433);
INSERT INTO `sub_category` VALUES (2, 'N', 2, 'รองเท้าส้นแบน', 1, 'Y', '2013-06-13 00:39:44', 0x323031332d30362d31332030303a34303a3237);
INSERT INTO `sub_category` VALUES (3, 'N', 3, 'รองเท้าแตะ', 1, 'Y', '2013-06-13 00:40:02', 0x323031332d30362d31332030303a34303a3237);
INSERT INTO `sub_category` VALUES (4, 'N', 4, 'รองเท้าหุ้มส้น', 1, 'Y', '2013-06-13 00:40:19', 0x323031332d30362d31332030303a34303a3237);
